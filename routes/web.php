<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/', 'HomeController@index')->name('home');

// User Profile
Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileController@index')->name('profile.index');
        Route::patch('/profile/{user}', 'ProfileController@update')->name('profile.update');
});

// Search talent 
Route::group(['prefix' => 'search'], function () {
    Route::get('/', 'SearchController@index')->name('search.index');
    Route::get('/about/{id}', 'SearchController@show')->name('search.show');
});

// Talent Mall 
Route::group(['prefix' => 'talent'], function () {
    Route::get('/mall', 'TalentMallContoller@index')->name('talent.index');
    Route::get('/mall/{id}', 'TalentMallContoller@show')->name('talent.show');
    Route::post('/ajax', 'TalentMallContoller@getTalentByCat')->name('search.ajax');
});
 
Route::group(['prefix' => 'seller'], function () {
    Route::get('/', 'SellerController@index')->name('seller.index');
    // Products
    Route::resource('/product', 'ProductController')->except(['update', 'destroy']);
    Route::post('/product', 'ProductController@store')->name('seller.product.store');
    Route::get('/sales', 'SalesController@index')->name('seller.index');
    Route::get('/sales', 'CommercialAdsController@index')->name('seller.index');
    
    
});

Auth::routes();
