<div>
    <h3 class="no-margin">saller</h3>
    <!--seller dashboard sidebar-->
    <div class="s-list">
        <div class="text-center">
            <div>
                <a href="/seller/account-setting" class="outline-none">
                    <img class="profile-circle-n" src="" />
                </a>
            </div>
        </div>
        <div class="text-center s-list1">
            <a class="mb-list inbox-l inbox-color" href="/message">
                <i class="fa fa-envelope profile-icon" aria-hidden="true"></i> INBOX
                <span style="color:#ff503f"> (0) </span>

            </a>

            <a class="mb-list inbox-r inbox-color cursor-pointer">
                <i class="fa fa-trophy profile-icon" aria-hidden="true"></i> AWARDS
                <span class="bkt"> (12)</span>
            </a>
        </div>
        <div class="clearfix"></div>

        <ul class="list-group s-g-item-1">
            <a class="inbox-color" href="/seller/sale">
                <li class="list-group-item"><i class="fa fa-bar-chart profile-icon" aria-hidden="true"></i> SALES <span class="badge">20</span></li>
            </a>
            <a role="button" class="inbox-color">
                <li class="list-group-item"><i class="fa fa-download profile-icon" aria-hidden="true"></i> DOWNLOADS <span class="badge">10</span></li>
            </a>
        </ul>

        <div class="text-center" style="margin-bottom:20px;">
            <a class="btn btn-danger btn-setting" href="/seller/account-setting">PROFILE SETTING</a>
        </div>
    </div>
    <!--end of seller dashboard sidebar-->
</div>