<h3 class="buyer-side-bar-heading"> Test Last</h3>
<div class="s-list">
    <div class="text-center">
        <div class="profile-circle" style="background-image:url('assets/images/profile.png')">
        </div>
        <!-- <img style="background-color: whitesmoke;" class="profile-circle" src=""> -->
    </div>

    <div class="text-center s-list1">
        <a class="mb-list inbox-l inbox-color">
            <i class="fa fa-shopping-cart profile-icon" aria-hidden="true"></i> PURCHASES
            <span class="bkt"> (1)</span>
        </a>
    </div>
    <div class="clearfix"></div>
    <div class="text-center">
        <a class="btn btn-danger btn-setting buyer-edit-button">EDIT ACCOUNT </a>
    </div>
    <div class="text-center">
        <a class="btn btn-danger  buyer-msg-button">MESSAGES</a>
    </div>
</div>