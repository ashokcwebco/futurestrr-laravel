@extends('layouts.talent') @section('content')
<!-- Start Banner -->
<section class="wow fadeIn cover-background background-position-top top-space" style="background-image:url({{asset('assets/images/homepage-5-slider-img-2.jpg')}});">
  <div class="opacity-medium bg-extra-dark-gray"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
        <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
          <!-- start page title -->
          <h1 class="alt-font text-white font-weight-600 mb-2">Talent Mall</h1>
          <!-- end page title -->
          <!-- start sub title -->
          <span class="display-block text-white opacity6 alt-font">
          Browse and Purchase Talent</span>
          <!-- end sub title -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Banner -->
<!-- Start Content -->
<section>
  <div class="container">
    <div class="row">
      <!-- Start left side content -->
      <aside class="col-sm-3">
        <!-- start Filter -->
        <div class="text-extra-dark-gray text-uppercase font-weight-600 text-small aside-title mb-2">
          <span>Filter Price</span>
        </div>
        <input type="range" class="custom-range w-100 mb-4 border-0" id="customRange1"  name="ram"  min="0" max="100" value="0">
        <p>Value: 1000</p>
        <!-- End Filter -->
        <!-- Start Categories -->
        <div class="text-extra-dark-gray text-uppercase font-weight-600 text-small aside-title mb-4" ><span>Categories</span></div>
        <ul class="list-style-6 text-small text-left mb-4" >
        @foreach($catagories as $catagory)
            @php $checked = $id == $catagory->id? 'checked':''; @endphp
          <li class="d-flex justify-content-between align-items-center pr-0 @if($id == $catagory->id) custom-disabled disabled @endif">{{ $catagory->name}}
            <input class="w-auto mb-0 case catagoryInputs" {{$checked}} type="checkbox" name="catagories[]" value="{{$catagory->id}}">
          </li>  
        @endforeach
          <li class="d-flex justify-content-between align-items-center pr-0">All Categories
              <input  type="checkbox" id = "selectAll"   value="" class="w-auto mb-0" type="checkbox">
          </li>
        </ul>
            <!-- End Categories --> 
            <!-- Start Award Rating -->
            <div class="text-extra-dark-gray text-uppercase font-weight-600 text-small aside-title mb-4"><span>Award Rating</span></div>
            <ul class="list-style-6 text-small">
              <li class="d-flex justify-content-between align-items-center pr-0" >
                <input  type="checkbox"  class="w-auto mb-0 case">
                <label class="mb-0" >
               </label>
             </li>
           </ul>   
         </aside>
         <!-- End Left side  -->
         <main class="col">
          <div class="row">
            <div class="col-md-12 no-padding xs-padding-15px-lr">
              <div id="talent-mal-list" class="filter-content overflow-hidden">
                <ul  class="portfolio-grid work-3col gutter-medium hover-option6 lightbox-portfolio">
                  <li class="grid-sizer" style="display:none;"></li>
                  <!-- start portfolio-item item -->
                  @foreach($talents as $talent)
                  <li class="design web photography grid-item wow fadeInUp last-paragraph-no-margin"  >
                    <figure>
                      <!-- <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden" >
                      <video style="width: 100%" height="245" id="video" >
                        <source src="" type="video/mp4">
                      </video>
                     </div> -->
                     @if($talent->image_path)
                     <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden">
                        <a href="javascript:void(0);"  class="cursor">
                         <img src="{{ asset('storage/uploads/'.$talent->image_path)}}"/>
                       </a>
                     </div>
                    @elseif($talent->commercialMedia[0]->image_path || $talent->sampleMedia[0]->sample_video)
                     <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden" >
                      <video style="width: 100%" height="245" id="video" >
                        <source  src="{{asset('storage/'.$talent->commercialMedia[0]->image_path??$talent->sampleMedia[0]->sample_video)}}" type="video/mp4">
                      </video>
                     </div> 
                     @endif
                     <figcaption class="bg-white p-3 border border-top-0">
                      <div class="portfolio-hover-main">
                        <div class="portfolio-hover-box">
                          <div class="portfolio-hover-content position-relative text-left">
                            <a href="javascript:void(0);" ><span class="line-height-normal font-weight-600 text-small margin-5px-bottom text-extra-dark-gray display-block">{{$talent->title}}</span></a>
                            <p class="text-medium-gray text-extra-small mb-0">{{ str_limit($talent->product_info,180) }}</p>
                            <p class="d-flex justify-content-between text-primary border-top pt-1 mt-2"><strong>${{$talent->price}}</strong> <a class="text-uppercase text-small">View more</a></p>
                          </div>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </li>
                @endforeach
              </ul>
              <div  class="wow fadeInUp mt-5 pagination-row">
                <div class="pagination text-small text-uppercase text-extra-dark-gray d-flex justify-content-center w-100">
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div  class="row">
          <div class="noProductFound">
            <h3> Sorry! No Products Found. </h3>
          </div>
        </div> -->
      </main>
    </div>
  </div>
</section>
<a class="scroll-top-arrow" href="javascript:void(0);" style="display: inline;"><i  class="ti-arrow-up"></i></a>
@endsection
@section('javascript')
  <script>
    $(document).ready(function(){
      var postData = {};
      $('input[type=checkbox]').on('change',function(){
        postData ['selectedCat'] = $('input[type=checkbox]:checked').map(function(_, el) {
            return $(el).val();
        }).get();
        postData["_token"] = "{{ csrf_token() }}";
        $.ajax({
          type:'POST',
          url:"{{ route('search.ajax')}}",
          data:postData,
          success:function(data){
            $('#talent-mal-list').html('');
            $('#talent-mal-list').html($.parseHTML(data));
          }
        });
      });   
    });
  </script>
@stop