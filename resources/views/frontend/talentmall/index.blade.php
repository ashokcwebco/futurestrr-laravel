@extends('layouts.talent') @section('content')
<!-- banner start -->
<section class="wow fadeIn cover-background background-position-top top-space" style="background-image:url({{ asset('assets/images/talent-mall.jpg')}});">
  <div class="opacity-medium bg-extra-dark-gray"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large">
        <div class="display-table-cell vertical-align-middle text-center padding-30px-tb">
          <!-- start page title -->
          <h1 class="alt-font text-white font-weight-600 mb-2">Talent Mall</h1>
          <!-- end page title -->
          <!-- start sub title -->
          <span class="display-block text-white opacity6 alt-font">
          Browse and Purchase Talent</span>
          <!-- end sub title -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End banner  -->
<!-- Start Content  -->
<section class="wow fadeIn pt-5"> 
  <!-- start filter content -->
  <div class="container " >
    <div class="row">
      <div class="col-md-12 no-padding xs-padding-15px-lr">
        <div class="filter-content overflow-hidden talent-mall">
          <ul class="portfolio-grid portfolio-metro-grid work-4col hover-option5 gutter-large" >
            <li class="grid-sizer" ></li>
            <!-- start portfolio item -->
            @foreach($catagories as $catagory)
            <li class="grid-item web advertising wow zoomIn tallent-mall " >
              <a href="{{ route('talent.show',$catagory->id)}}">
                <figure>
                  <div class="portfolio-img" >
                    <img  title="" src="{{ asset( $catagory->catagory_image_path)}}" />
                    <!-- <img   title="" src="{{asset('assets/images/talent-mall/no_image-200x200.jpg')}}" /> -->
                  </div>
                  <figcaption>
                    <div class="portfolio-hover-main text-center" > 
                      <div class="portfolio-hover-box vertical-align-middle">
                        <div class="portfolio-hover-content position-relative last-paragraph-no-margin">
                          <div class="bg-deep-pink center-col separator-line-horrizontal-medium-light2 position-relative"></div>
                          <span class="font-weight-600 letter-spacing-1 alt-font text-white text-uppercase margin-5px-bottom display-block">{{$catagory->name}}</span>
                        </div>
                      </div>
                    </div>
                  </figcaption>
                </figure>
              </a>
            </li>
            @endforeach
            <!-- End portfolio item -->
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- end filter content -->
</section>
<!-- End content -->
<a class="scroll-top-arrow" href="javascript:void(0);" style="display: inline;"><i  class="ti-arrow-up"></i></a>
@endsection