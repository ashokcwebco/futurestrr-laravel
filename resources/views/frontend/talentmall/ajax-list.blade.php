<ul class="portfolio-grid work-3col gutter-medium hover-option6 lightbox-portfolio">
    <li class="grid-sizer" style="display:none;"></li>
    <!-- start portfolio-item item -->
    @foreach($talents as $talent)
    <li class="design web photography grid-item wow fadeInUp last-paragraph-no-margin"  >
      <figure>
        <!-- <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden" >
        <video style="width: 100%" height="245" id="video" >
          <source src="" type="video/mp4">
        </video>
        </div> -->
        @if($talent->image_path)
        <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden">
          <a href="javascript:void(0);"  class="cursor">
            <img src="{{ asset('storage/uploads/'.$talent->image_path)}}"/>
          </a>
        </div>
      @elseif($talent->commercialMedia[0]->image_path)
        <div class="portfolio-img bg-extra-dark-gray position-relative text-center overflow-hidden" >
        <video style="width: 100%" height="245" id="video" >
          <source  src="{{asset('storage/'.$talent->commercialMedia[0]->image_path)}}" type="video/mp4">
        </video>
        </div> 
        @endif
        <figcaption class="bg-white p-3 border border-top-0">
        <div class="portfolio-hover-main">
          <div class="portfolio-hover-box">
            <div class="portfolio-hover-content position-relative text-left">
              <a href="javascript:void(0);" ><span class="line-height-normal font-weight-600 text-small margin-5px-bottom text-extra-dark-gray display-block">{{$talent->title}}</span></a>
              <p class="text-medium-gray text-extra-small mb-0">{{ str_limit($talent->product_info,180) }}</p>
              <p class="d-flex justify-content-between text-primary border-top pt-1 mt-2"><strong>${{$talent->price}}</strong> <a class="text-uppercase text-small">View more</a></p>
            </div>
          </div>
        </div>
      </figcaption>
    </figure>
  </li>
  @endforeach
</ul>
<div  class="wow fadeInUp mt-5 pagination-row">
  <div class="pagination text-small text-uppercase text-extra-dark-gray d-flex justify-content-center w-100">
  {{ $talents->links()}}
  </div>
</div>