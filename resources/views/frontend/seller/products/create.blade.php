@extends('layouts.seller') @section('content')
    <h3>Add Product</h3>
    <div class="well panel panel-danger panel-m">
        <div class="panel-body">
            {!! Form::open(['route' => 'seller.product.store']) !!} @include('frontend.seller.products.forms.form')
            <hr>
            <div class="pull-right">
                <button type="submit" class="btn btn-danger">SAVE
                </button>
                <button type="button" class="btn btn-default btn-d" href="/seller/dashboard">CANCEL</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>        
@endsection