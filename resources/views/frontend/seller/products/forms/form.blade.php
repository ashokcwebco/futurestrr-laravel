<div class="form-group">
    {!! Form::label('talent_category_id','Select Category of Product') !!}
    {!!Form::select('talent_category_id', $catagories, null, ['class' => 'form-control'])!!}
    
</div>
<div class="form-group">
    {!! Form::label('title','Title') !!}
    {!! Form::text('title', old('title') , ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''),'placeholder'=>'Title' ]) !!}
    {!! $errors->first('display_name', '<span class="alert alert-danger" role="alert">:message</span>') !!}
</div>
<div class="form-group p-b-upload">
    {!! Form::label('commercial','Upload Image or Video Commercial of Product') !!}
    <div class="file-upload">
        <div class="file-select">
            <div class="file-select-button" id="fileName">Browse</div>
            <div  class="file-select-name" id="noFile">No file selected</div>
         
            {!! Form::file('commercial', null) !!}
            <!-- file list -->
        </div>

       
    </div>
    {!! $errors->first('commercial', '<span class="alert alert-danger" role="alert">:message</span>') !!}
</div>

<div class="form-group p-b-upload">
    {!! Form::label('commercial','Upload Audio or Video Sample of Product') !!}
    <div class="file-upload">
        <div class="file-select">
            <div class="file-select-button" id="fileName">Browse</div>
            <div  class="file-select-name" id="noFile">No file selected </div>
            
            {!! Form::file('sample_video', null) !!}
        </div>

        
    </div>

</div>

<div class="form-group">
{!! Form::label('product_info','Bio Information of Product') !!}
{!! Form::textarea('product_info', old('product_info') , ['class' => 'form-control' . ($errors->has('product_info') ? ' is-invalid' : ''),'placeholder'=>'Bio Information of Product','rows'=>'4' ]) !!}
{!! $errors->first('product_info', '<span class="alert alert-danger" role="alert">:message</span>') !!}
</div>

<div class="form-group">
{!! Form::label('description','Bio Information of Seller') !!}
{!! Form::textarea('description', old('description') , ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''),'placeholder'=>'Bio Information of Seller','rows'=>'4' ]) !!}
{!! $errors->first('description', '<span class="alert alert-danger" role="alert">:message</span>') !!}
</div>
<!--<br>-->
{!! Form::label('description','Price of Product') !!}

<div class="input-group">
    <span class="input-group-addon"><i class="fas fa-usd" aria-hidden="true"></i></span>
    {!! Form::number('price', old('price') , ['class' => 'form-control' . ($errors->has('price') ? ' is-invalid' : ''),'placeholder'=>'Price' ]) !!}
</div>
    {!! $errors->first('price', '<span class="alert alert-danger" role="alert">:message</span>') !!}

<br>

<div class="form-group">
    <label>Upload Product <i class="text-p">(Mp3,jpeg,video,pdf)</i></label>
    <div class="file-upload">
        <div class="file-select">
            <div class="file-select-button" id="fileName">Browse</div>
            <div class="file-select-name" id="noFile">No file selected</div>
           
            {!! Form::file('pdf', null) !!}
        </div>
        <ul>
        <li><div class="row">
            <div class="col-md-4"><strong>tumb.png</strong></div><div class="col-md-4"></div><div class="col-md-4"><a href="javascript:;"><i class="fa fa-times" style="color:#ff503f;"></i></a></div>
        </div>
        </li>
        </ul>

       
    </div>

</div>
<div class="upload-products social-media-icons">
    <div class="row">
        <div class="col-sm-12">
        {!! Form::label('facebookLink','Social media links') !!}
            <div class="form-group">
                <div class="col-sm-1 icon">
                    <i class="fab fa-facebook-f facebook-icon-large" aria-hidden="true"></i>
                </div>
                <div class="input-group col-sm-11">
                {!! Form::text('facebookLink', old('facebookLink') , ['class' => 'form-control' . ($errors->has('facebookLink') ? ' is-invalid' : ''),'placeholder'=>'facebook link' ]) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">

                <div class="col-sm-1 icon">
                    <i class="fab fa-instagram facebook-icon-large" aria-hidden="true"></i>
                </div>
                <div class="input-group col-sm-11">
                {!! Form::text('instagramLink', old('instagramLink') , ['class' => 'form-control' . ($errors->has('instagramLink') ? ' is-invalid' : ''),'placeholder'=>'instagram link' ]) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">

                <div class="col-sm-1 icon">
                    <i class="fab fa-twitter facebook-icon-large" aria-hidden="true"></i>
                </div>
                <div class="input-group col-sm-11">
                {!! Form::text('twitterLink', old('twitterLink') , ['class' => 'form-control' . ($errors->has('twitterLink') ? ' is-invalid' : ''),'placeholder'=>'twitter link' ]) !!}
                </div>
            </div>
        </div>
    </div>
</div>