@extends('layouts.seller')
@section('content')
<h3>Sales</h3>

<div class="" style="margin-top:20px">
    <div class="row">
        <div class="col-sm-6">
        <div class="well" >
            <span class="">Daily Sales <span class="text-col">0</span></span>
            <div class="icon-c">
                <img class="img-c" src="{{ asset('assets/images/sales.png') }}" />
            </div>

        </div>
    </div>

    <div class="col-sm-6">
        <div class="well">
            <span class="sale-t">Today's Revenue: <span class="text-col">$ 0</span></span>
            <div class="icon-c">
                <img class="img-c" src="{{ asset('assets/images/today-revenue.png') }}" />
            </div>
        </div>
    </div>
    </div>

    <div class="row">
         <div class="col-sm-6">
        <div class="well">
             <span class="sale-t">Downloads <span class="text-col">0</span></span>
             <div class="icon-c">
                <img class="img-c" src="{{ asset('assets/images/download.png') }}" />
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="well">
             <span class="sale-t">Total Revenue <span class="text-col">$ 0</span></span>
             <div class="icon-c">
                <img class="img-c" src="{{ asset('assets/images/total-revenue.png') }}" />
            </div>
        </div>
    </div>
    </div>
</div>
@endsection




