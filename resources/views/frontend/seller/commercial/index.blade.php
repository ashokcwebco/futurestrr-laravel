
<h3>Purchase Commercial Ads</h3>

<div class="well panel panel-danger panel-m">

    <div class="panel-body">
        <div class="row">
            @foreach($plans as $plan)
            <div class="col-sm-4" >
                <div class="well well-card text-1">
                    <h4 class="card-text">{{ $plan->title}}</h4>
                    <p class="card-para">{{$plan->description}}</p>
                    <h4 class="price-f">$ {{$plan->price}}</h4>
                    <button  type="button" class="btn btn-danger btn-margin">BUY NOW</button>
                </div>
            </div>
            @endforeach
        </div>

        <!-- <div  class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <a href="/seller/commercial-ad-dashboard" class="btn btn-danger">UPGRADE PLAN</a>
            </div>
        </div> -->

        <div class="f-g-margin">
            <div class="row">
                <div class="col-sm-5 col-sm-width-box">
                    <hr class="hr-box">
                </div>
                <div class="col-sm-2 col-sm-width-box1">
                    <span class="text-color">or</span>
                </div>
                <div class="col-sm-5 col-sm-width-box">
                    <hr class="hr-box">
                </div>
            </div>
        </div>

        <!-- <div class="plan-h-d">
            <form novalidate #plan="ngForm" (ngSubmit)="addCustomPlan(plan)">
                <div class="form-group f-g-margin">
                    <label for="">Select custom package option</label>
                    <textarea type="text" name="description" [(ngModel)]="plan.description" #description="ngModel" class="form-control custom-pkg"
                        placeholder="Message Text"></textarea>
                    <span class="text-4">Purchase Commercial Ads for users to see on FutureStarr!! Your ad will appear on
                        the Talent Mall Page
                        <a role="button" class="text-c-o" (click)="showContactServiceModal()">Contact Us</a></span>
                </div>

                <div class="btn-ads">
                    <button type="submit" class="btn btn-danger" [disabled]="!plan.description">SEND</button>
                    <button type="button" class="btn btn-default btn-d" (click)="goBack()">CANCEL</button>
                </div>
            </form>
        </div> -->
    </div>
</div>


<!--create promotion modal-->

<!--  Modal -->
<div class="modal fade" id="commercialModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Checkout</h4>
            </div>
            <div class="modal-body m-b-padding">

                <div class="table-margin table-responsive">

                    <table class="table">
                        <thead class="">
                            <tr>
                                <th class="plan-p">Plan</th>
                                <th class="plan-p">Details</th>
                                <th class="plan-p">Price</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="plan-p">{{plan.plan_name}}</td>
                                <td class="plan-p">{{plan.description}}</td>
                                <td class="plan-p">${{plan.price}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <hr class="total-b">
                <span class="grand-total">Grand Total</span>
                <span class="total-amount">${{plan.price}}</span>
                <!--</div>-->
            </div>

            <div class="modal-footer">
                <button type="button" (click)="checkedOut(plan.price, plan.plan_name, plan.id)" class="btn btn-pay">
                    <img class="btn-img" src="assets/images/paypal-6.png" />
                    <span class="text-4">Check out</span></button>
            </div>
        </div>
    </div>
</div>

<!--create contact service modal-->

<!--  Modal -->
<div class="modal fade" id="contactServiceModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form novalidate #contactForm="ngForm" (ngSubmit)="addContact(contact)">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title heading-c">Customer Service</h4>
                </div>
                <div class="modal-body">
                    <div class="f-g-padding">
                        <div class="form-group">
                            <input type="text" class="form-control f-c-radius" name="username" [(ngModel)]="contact.username" #myname="ngModel" placeholder="Name*"
                                required>
                            
                        </div>
                        <div class="form-group ">
                            <input type="email" class="form-control f-c-radius" name="email" [(ngModel)]="contact.email" #myemail="ngModel" placeholder="Email*"
                                required>
                        </div>
                        <div class="form-group">
                            <textarea rows="4" type="text" class="form-control f-c-radius" name="productInfo" [(ngModel)]="contact.productInfo" placeholder="Product Information"></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger" [disabled]="!(contact.username && contact.email)">SAVE</button>
                    <button type="button" class="btn btn-default btn-d" data-dismiss="modal">CANCEL</button>
                </div>
            </form>
        </div>
    </div>
</div>