@extends('layouts.talent') @section('content')
<section class="wow fadeIn cover-background background-position-center top-space" style="background-image:url('assets/images/star-search.jpg');">
    <div class="opacity-medium bg-extra-dark-gray"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 display-table page-title-large text-center">
                <h1 class="alt-font text-white font-weight-600 mb-2">Starr Search</h1>
                <span class="display-block text-white opacity6 alt-font">
                Browse and Purchase Talent</span>
            </div>
        </div>
    </div>
</section>

<section class="star-serach-text-sec">
    <div class="container text-center">
        <div class="text-center mb-3">
            <h1 class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">Welcome to the #1 Online Talent Marketplace</h1>
            <span class="separator-line-horrizontal-medium-light2 bg-deep-pink display-table margin-auto width-100px"></span>
        </div>
        <p class="color-black">If you have an active social media presence with Tweets, Facebook Pages, and ads, but not accumulating any followers. Or, if your Instagram uploads are not getting you much awaited and desired audiences… It’s time for you to try something new and Sign up with Future Starr. Future Stars is a premier talent marketing platform which enables creative talents across the globe to share, showcase, and sell their passion through innovative ways. Whether you are fitness trainer wanting to sell your workout videos or nutrition plans, or a stellar artist in search of your passionate audience, or a local author with a mission to raise a platform for your writings or lectures, our Starr Search feature will help you promote and market your talent and be found by one and all. Future Starr is an online talent distribution platform where you can promote and turn your talent aspirations into a full-time career.</p>
        <!-- <p ><a href="javascript:void(0);" class="btn btn-small btn-primary text-very-small border-radius-4">Sign up today!</a></p> -->
    </div>
</section>

<section class="pt-0 wow fadeIn animated">
    <div class="container-fluid">
    <div class="row h-100">
    @php $j=0; $i=0; @endphp
    @foreach($catagories as $key=>$catagory)
            <div class="col-lg-6 mb-4 mb-sm-0">
                <div class="row h-100">
                    @if($i <= 1 && $j < 1)
                    <div class="col-sm-6 p-0 xs-height-300px cover-background position-relative wow fadeIn" style="background: transparent url({{ asset($catagory->catagory_banner)}}" >
                    </div>
                    <div class="d-flex flex-column col-sm-6 p-5 position-relative bg-extra-dark-gray wow fadeIn" data-wow-delay="0.2s">
                        <div class="alt-font text-extra-large text-white mb-3">{{$catagory->name}}</div>
                        <p >{!!str_limit($catagory->catagory_desc, 190)!!}</p>
                        <div class="mt-auto">
                            <a href="{{ route('search.show',$catagory->id)}}" class="btn btn-transparent-white btn-small border-radius-4"><i class="fa fa-play-circle icon-very-small mr-2" aria-hidden="true"></i>Read More
                            </a>
                        </div>
                    </div>
                    @elseif($j <= 2)
                    <div class="order-2 order-lg-1 d-flex flex-column col-sm-6 p-5 position-relative bg-extra-dark-gray wow fadeIn" data-wow-delay="0.2s">
                        <div class="alt-font text-extra-large text-white mb-3">{{$catagory->name}}</div>
                        <p>{!! str_limit($catagory->catagory_desc, 190) !!}</p>
                        <div class="mt-auto">
                            <a href="{{ route('search.show',$catagory->id)}}" class="btn btn-transparent-white btn-small border-radius-4" >
                                <i class="fa fa-play-circle icon-very-small mr-2" aria-hidden="true"></i>Read More
                            </a>
                        </div>
                    </div>
                    <div class="order-1 order-lg-2 col-sm-6 p-0 xs-height-300px cover-background position-relativewow fadeIn"  style="background: transparent url({{ asset($catagory->catagory_banner)}}"></div>
                    @endif
                </div>
            </div>
            @php 
                if($j == 2){
                    $i=0;$j=0;
                }elseif($i == 1){
                    $j++;
                }elseif($i< 1){
                    $i++;
                }
            @endphp
        @endforeach
        </div>
        <div class="wow fadeInUp mt-5">
            <div class="pagination text-small text-uppercase text-extra-dark-gray d-flex justify-content-center w-100">
            {{ $catagories->links() }}
          </div>
        </div>
    </div>

</section>
<a class="scroll-top-arrow" href="javascript:void(0);" style="display: inline;"><i  class="ti-arrow-up"></i></a>

<!-- buy your talent modal -->
<div class="modal-ask-to-login fade" id="askToJoinAsBuyer" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="ask-to-login">
                <div class="modal-body">
                    <br />
                    <h3 class="ask-register"> To use this feature please register as Buyer or Seller. <br /> <small>By clicking Register, you will be logged out from your current account. </small></h3>
                    <!-- <i class="fa fa-circle-o-notch fa-spin"></i> -->
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-danger" data-dismiss="modal" >REGISTER</button>
                    <button type="button" class="btn btn-default btn-d" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end of buy your talent modal -->

<!-- ASK TO LOGIN -->
<div class="modal-ask-to-login fade" id="askToLogin" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="ask-to-login">
                <div class="modal-body">
                    <div class="form-group text-spinner">
                        <h3 class="deleteConfirmation">Please login to use this feature! </h3>
                        <i class="fa fa-circle-o-notch fa-spin"></i>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
