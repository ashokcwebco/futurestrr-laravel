@extends('layouts.talent') @section('content')
<!-- start page title section -->
        <section  class="wow fadeIn cover-background background-position-center top-space" style="background:url({{ asset($catagory->catagory_detailed_banner)}})" style="min-height: 308px">
            <div class="opacity-medium bg-extra-dark-gray"></div>
        </section>
        <!-- end page title section -->
        
        <section class="wow fadeIn py-3">
            <div class="container">
                <h1 class="alt-font text-black font-weight-600 text-center mb-0">{{$catagory->name}}</h1>
            </div>
        </section>
        
        <!-- start accordion section -->
        <section class="bg-light-gray border-none wow fadeIn pb-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft mb-5">
                        <span class="text-center alt-font d-block text-extra-dark-gray font-weight-500 mb-5"> {!! $catagory->catagory_desc !!}</span>
                        <div class="text-center mt-5">
                            <a class="btn btn-small btn-transparent-black" href="javascript:void(0);" >PURCHASE NEW TALENT</a> 
                            <a class="btn btn-small btn-dark-gray" href="javascript:void(0);" style="margin-left:4px;" >SELL YOUR TALENT</a>
                        </div>
                    </div>
                    <div class="col-md-6 cover-background sm-height-auto xs-height-350px wow fadeInRight" style="background: url({{ asset($catagory->catagory_image_path)}})"></div>
                </div>
            </div>
        </section>
        <!-- end accordion section -->

        <!-- start social icons style 02 section -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7 col-sm-12 col-xs-12 text-center mb-5">
                        <div class="position-relative overflow-hidden width-100">
                            <span class="text-small text-outside-line-full alt-font font-weight-600 text-uppercase">SHARE IT ON</span>
                        </div>
                    </div>
                    <div class="col-md-12 text-center elements-social social-icon-style-4">
                        <ul class="medium-icon">
                            <li><a class="facebook" href="javascript:void(0);" shareButton="facebook" ><i class="fab fa-facebook-f"></i><span></span></a></li>
                            <li><a class="twitter" href="javascript:void(0);" shareButton="twitter"><i class="fab fa-twitter"></i><span></span></a></li>
                            <!-- <li><a class="google" href="javascript:void(0);" shareButton="google" [sbUrl]="repoUrl"><i class="fa fa-google-plus"></i><span></span></a></li> -->
                            <li><a class="linkedin" href="javascript:void(0);" shareButton="linkedin"><i class="fab fa-linkedin"></i><span></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- end social icons style 02 section -->

<a class="scroll-top-arrow" href="javascript:void(0);" style="display: inline;"><i  class="ti-arrow-up"></i></a>

<!-- ASK TO LOGIN -->
<div class="modal-ask-to-login fade" id="askToLogin" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="ask-to-login">
                <div class="modal-body">
                    <div class="form-group text-spinner">

                        <h3 class="deleteConfirmation">Please login to use this feature! </h3>
                        <i class="fa fa-circle-o-notch fa-spin"></i>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>

<!-- Ask to join as Seller -->

<div class="modal-ask-to-login fade" id="askToJoinAsSeller" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="ask-to-login">
                <div class="modal-body">
                    <br />
                    <h3 class="ask-register"> To use this feature please register as Seller. <br /> <small>By clicking Register, you will be logged out from your current account. </small></h3>
                    <!-- <i class="fa fa-circle-o-notch fa-spin"></i> -->
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-danger" data-dismiss="modal" >REGISTER</button>
                    <button type="button" class="btn btn-default btn-d" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Ask to join as Buyer -->

<div class="modal-ask-to-login fade" id="askToJoinAsBuyer" role="dialog">
    <div class="modal-dialog">
        <form>
            <!-- Modal content-->
            <div class="ask-to-login">
                <div class="modal-body">
                    <br />
                    <h3 class="ask-register"> To use this feature please register as Buyer. <br /> <small>By clicking Register, you will be logged out from your current account. </small></h3>
                    <!-- <i class="fa fa-circle-o-notch fa-spin"></i> -->
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-danger" data-dismiss="modal"  >REGISTER</button>
                    <button type="button" class="btn btn-default btn-d" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
