<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'FutureStarr') }}</title>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <!-- <link href="{{ asset('assets/css/global.css') }}" rel="stylesheet"> -->
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css">

    <!-- <link href="{{ asset('assets/css/device-only.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/ng2-toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/video-js.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/et-line-icons.css') }}" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href="{{ asset('style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/register.css') }}" rel="stylesheet"><!-- <script src="{{ asset('assets/js/jquery.min.js') }}" crossorigin="anonymous"></script> -->
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="{{ asset('assets/js/video.min.js') }}"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/isotope.pkgd.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}" type="text/javascript"></script> -->
    <div id="load"></div>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-39143753-1');
        new WOW().init();
    </script>
    <!-- <link href="{{ asset('assets/css/emojiconarea.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/js/emojionearea.min.js') }}" type="text/javascript"></script> -->
</head>
<body>
        @include('layouts.talent.header')
		    @yield('content')
	    @include('layouts.talent.footer')
    <script>
    document.onreadystatechange = function () {
        var state = document.readyState
        if (state == 'complete') {
            setTimeout(function(){
                document.getElementById('interactive');
                document.getElementById('load').style.visibility="hidden";
            },1000);
        }
    }
    </script>
    @yield('javascript')
    <script src="{{ asset('assets/js/custom.js') }}" type="text/javascript"></script>
</body>
</html>
