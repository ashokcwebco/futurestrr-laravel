<footer style="background-color:#151829;">
  <div class=" container footer-s">
    <div class="row footer">

      <div class="col-sm-3">
        <h4 class="footer-s">Quick Links</h4>
        <p><a href="/">Home</a></p>
        <p><a href="/about-us">About Us</a></p>
        <p><a href="/star-search">Starr Search</a></p>
        <p><a href="/talent-mall">Talent Mall</a></p>
        <p><a href="/blog">Blog</a></p>
        <p><a href="/contact-us" style="color:#777;font-size:13px;">Contact Us</a></p>
      </div>

      <div class="col-sm-3">
        <h4 class="footer-s">Terms & Privacy</h4>
        <p><a href="/privacy-policy">Privacy Policy</a></p>
        <p><a href="/term-conditions">Terms and Conditions</a></p>
        <p><a href="/refund-policy">Refund Policy</a></p>
      </div>

      <div class="footer-s" class="col-sm-3">
        <h4>Contact us</h4>

        <p>FUTURESTARR MEDIA LLC.</p>
        <p>3620 Piedmont Rd NE,</p>
        <p>Ste B 5191, Atlanta,</p>
        <p>GA 30305</p>
      </div>

      <div class="col-sm-3">
        <h4 class="footer-s">Connect with Us</h4>
        <div class="footer-box">
          <ul class="social-icon">
            <li><a data-toggle="tooltip" title="Facebook" href="https://www.facebook.com/FutureStarrcom/" target="_blank"><i
                  class="fa fa-facebook"></i></a></li>
            <li><a data-toggle="tooltip" title="Twitter" href="https://twitter.com/futurestarrcom?lang=en/" target="_blank"><i
                  class="fa fa-twitter"></i></a></li>
            <!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
            <li><a data-toggle="tooltip" title="LinkedIn" href="https://www.linkedin.com/in/futurestarr/" target="_blank"><i
                  class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-12 footer-m">
        <p style="">© 2019, FutureStarr, All Rights Reserved</p>
      </div>
    </div>
  </div>
</footer>