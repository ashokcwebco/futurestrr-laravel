<nav class="navbar navbar-inverse fixed-top" >
@if (Auth::guest())
    <div class="container top-bg" style="max-width: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="social-media">
                        <li>
                            <a href="https://www.facebook.com/FutureStarrcom/" target="_blank" data-toggle="tooltip" title="Facebook">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/futurestarrcom?lang=en/" target="_blank" data-toggle="tooltip" title="Twitter">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/watch?v=1WzPnNBSKa8" target="_blank" data-toggle="tooltip" title="Youtube">
                            <i class="fab fa-youtube"></i></a>
                        </li>
                        <li>
                            <a href="{{ route('login') }}">Login</a>
                        </li>
                        <li style="color:#ffffff;">/</li>
                        <li>
                            <a href="{{ route('register') }}">Register</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    @endif


    <!--if remove the above container than add (.top-cls:50px;) using jquery or to add this class and remove above top-bg class for seller module-->

        <div class="container-fluid h-font">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" title="Future Starr">
                    <img class="img-responsive sm-logo" alt="futurestarr logo" src="{{ asset('assets/images/futurelogo.png')}}" />
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">

                <ul class="nav navbar-nav navbar-right">
                    <li data-toggle="collapse" data-target="#myNavbar">
                        <a href="{{URL::to('/')}}">
                            <div class="header-icon">
                                <img class="fa-icon-font" alt="futurestarr home" src="{{ asset('assets/images/home/home.png')}}"></div>
                            HOME
                        </a>
                    </li>   
                    <!-- <li data-toggle="collapse" data-target="#myNavbar">
                        <a style="margin-top: 11px">
                            <div class="header-icon dashbord-icon"><i class="fa fa-dashboard fa-icon-font"></i></div>
                            STARR SEARCH</a>
                         <a style="margin-top: 11px">
                            <div class="header-icon dashbord-icon"><i class="fa fa-dashboard fa-icon-font"></i></div>
                            DASHBOARD</a> 

                    </li> -->
                    <li data-toggle="collapse" data-target="#myNavbar">
                    <a href="#">
                               <div class="header-icon dashbord-icon"><i class="fas fa-dashboard fas-icon-font"></i></div>
                            DASHBOARD</a> 
                    </li>

                    <li>
                        <a href="{{ route('search.index')}}" >
                            <div class="header-icon"><img class="fa-icon-font" src="{{ asset('assets/images/home/starr_s.png') }}" alt="star icon"></div>
                            STARR SEARCH</a>
                    </li>
                    <li>
                        <a href="{{ route('talent.index')}}" >
                            <div class="header-icon"><img class="fa-icon-font" src="{{ asset('assets/images/home/tallent_m.png') }}" alt="tallent"></div>
                            TALENT MALL</a>
                    </li>
                    <li>
                        <a href="/social-buzz" >
                            <div class="header-icon"><img class="fa-icon-font" alt="social icon" src="{{ asset('assets/images/home/social_b.png') }}" alt="social icon"></div>
                            SOCIAL BUZZ</a>
                    </li>
                    <li>
                        <a href="/blog" >
                            <div class="header-icon"><img class="fa-icon-font" src="{{ asset('assets/images/home/blog.png') }}" alt="blog"></div>
                            BLOG</a>
                    </li>
                    <li>
                        <a href="/blog" >
                            <div class="header-icon"><img class="fa-icon-font" src="{{ asset('assets/images/home/blog.png') }}" alt="blog"></div>
                            CONTACT US</a>
                    </li>
                    <!-- <li data-toggle="collapse" data-target="#myNavbar">
                        <div  data-toggle="collapse" data-target="#myNavbar" >
                         <a href="/contact-us">
                            <div class="header-icon"><img class="fa-icon-font" src="assets/images/home/contact.png" alt="contact"></div>
                            CONTACT US</a>
                    </li-->
                    <!-- <li style="display: flex;padding-top: 6px">
                        <span>
                            <input class="superSearch search-filter" type="text" placeholder="Start typing to search.." />
                            <ul class="search-results" >
                            <li  class="search-results-heading">Talents <span class="search-result-counts"> 0 </span> </li>
                            <li class="search-results-items"> </li>
                            <li  class="search-results-error">No results found.</li>
                            <li class="search-results-heading">Categories <span class="search-result-counts"> 0 </span> </li>
                            <li class="search-results-items" >name</li>
                            <li class="search-results-error">No results found.</li>
                        </ul>
                        </span>
                        <a class="search-filter" style="padding-top: 10px; font-size: 16px;"  data-toggle="tooltip" title="Search">
                            <span class="glyphicon glyphicon-search search-icon"></span>
                        </a>
                    </li> -->
            <!-- <li>
                <span>
                        <span >
                            <button data-toggle="tooltip" title="Shipping Cart" type="button" class="btn btn-danger btn-sm b-btn cart-btn" href="/cart">
                                <span class="cart-count">2</span>
                <span class="glyphicon glyphicon-shopping-cart"></span>
                </button>
                </span>
                </span>
            </li> -->
            <!-- <li >
                <a class="dropdown-margin" role="button" data-toggle="dropdown">username
                        <span class="caret"></span>
                    </a>
                <ul class="dropdown-menu seller-menu">
                    <li data-toggle="collapse" data-target="#myNavbar">
                        <a role="button" href="/seller/account-setting">Account Info</a>
                    </li>
                    <li data-toggle="collapse" data-target="#myNavbar">
                        <a role="button" href="/seller/change-password">Security</a>
                    </li>
                    <li>
                        <a role="button" >Logout</a>
                    </li>
                </ul>
            </li> -->
            @if (!Auth::guest())
            <li>
                <a class="dropdown-margin" role="button" data-toggle="dropdown">my Account
                        <span class="caret"></span>
                    </a>
                <ul class="dropdown-menu seller-menu">
                    <li data-toggle="collapse" data-target="#myNavbar">
                        <a role="button" href="{{ route('profile.index')}}">Account Info</a>
                    </li>

                    <li data-toggle="collapse" data-target="#myNavbar">
                        <a role="button" href="/buyer/change-password-buyer">Security</a>
                    </li>

                    <li>
                    <a  href="{{ route('logout') }}"  onclick="event.preventDefault();  document.getElementById('logout-form').submit();"> {{ __('Logout') }} </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    </li>
                </ul>

            </li>
            @endif
            </ul>

        </div>
    </div> 
</nav>
<!-- <button id="showLoginBox"></button> -->
