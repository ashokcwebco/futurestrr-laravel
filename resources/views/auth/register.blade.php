@extends('layouts.talent')
@section('content')
<div class="container-fluid" style="height:275px;background-image:url('assets/images/header-bg.jpg');background-size:cover;">
</div>
<div class="container register-container">
    <div class="col-md-2"></div>
        <div class="col-sm-8 col-md-8 col-xs-12">
            <div class="col-sm-offset-4 col-sm-5 tabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#aaa" data-toggle="tab">
                            <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                            <br>Buyer</a>
                    </li>
                    <li>
                        <a href="#bbb" data-toggle="tab">
                            <i class="fa fa-address-book fa-2x" aria-hidden="true"></i>
                            <br>Seller</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="tabs">
                <div class="tab-pane arrow active" id="aaa" >
                    <div class="col-sm-8 col-xs-12 panel-part" style="background-color:#151829;">
                        <h3 class="register-panel" style="color:#fff;"> </h3>
                        {!! Form::open(['route' => 'register']) !!}
                        @csrf
                        <input type="hidden" name="role_id" value="3">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                    {!! Form::text('username', old('username') , ['class' => 'form-control' . ($errors->has('username') ? ' is-invalid' : ''),'placeholder'=>'User Name' ]) !!}
                                        {!! $errors->first('username', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                    {!! Form::email('email', old('email') , ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email' ]) !!}
                                        {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                    {!! Form::text('password', old('password') , ['class' => 'form-control' . ($errors->has('password') ? ' is-invalid' : ''),'placeholder'=>'Password' ]) !!}
                                        {!! $errors->first('password', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                    {!! Form::text('password_confirmation', old('password_confirmation') , ['class' => 'form-control' . ($errors->has('password_confirmation') ? ' is-invalid' : ''),'placeholder'=>'Re-Type Password' ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                                        <button class="btn btn-danger btn-lg" type="submit" style="background-color:#c9302c;float:right;margin-bottom:21px;">Register</button>
                                   
                                </div>
                            </div>
                            {!! Form::close()!!}
                    </div>


                    <div class="col-sm-4 col-xs-12  panel-part" style="background-color:#292F51">
                        <h1 class="text-left  panel-second-half-heading register-panel" style="color:#fff;">What buyers can do?</h1>
                        <p style="color:#fff;">
                            <i class="fa fa-user" aria-hidden="true"></i> Buyer can have their own account.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-comments" aria-hidden="true"></i> Buyer can send and receive messages from seller.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-money" aria-hidden="true"></i> Buyer can make purchases.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-cloud-download" aria-hidden="true"></i> Buyer can download products.</p>
                    </div>
                </div>
                <!-- first tab closed -->

                <!-- second tab -->
                <div class="tab-pane arrow1" id="bbb" >
                    <div class="col-sm-8 col-xs-12 panel-part buyer-register" style="background-color:#151829;">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12 col-md-12 fix-nit">
                                <div class="form-group re-stripe">
                                    <span style="color:#ffffff;">Create your Talent account with </span>
                                    <a href="https://dashboard.stripe.com/register" target="_blank" style="color:#008cdd;">Stripe</a>
                                </div>
                            </div>
                        </div>
                        {!! Form::open(['route' => 'register']) !!}
                            <div class="row">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control"  required pattern="[0-9a-zA-Z]+$" placeholder="User Name">
                                        <!-- <span class="alert alert-danger" >user name is required </span>
                                        <span class="alert alert-danger ">Only AlphaNumeric allowed </span> -->
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" required pattern="([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA])+$" placeholder="Email">
                                        <!-- <span class="alert alert-danger" >Email is required</span>
                                        <span class="alert alert-danger" >Invalid Email</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" class="form-control"  required placeholder="Password">
                                        <!-- <span class="alert alert-danger">Password is required </span> -->
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" class="form-control"  required placeholder="Re-Type Password">
                                        <!-- <span class="alert alert-danger" >Password not matched</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12 col-md-12">
                                        <button class="btn btn-danger btn-lg" type="submit" name="button">Create your FutureStarr Talent account</button>
                                  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12 col-md-12">
                                    <div class="form-group re-stripe" >
                                        <span style="color:#ffffff;">Already have stripe? Activate with FutureStarr</span>
                                        <a href="https://www.futurestarr.com/futuredev/connect" target="_blank" style="color:#ff503f;">here</a>
                                    </div>
                                    <div class="form-group re-stripe">
                                        <span class="text-success"><i class="fa fa-check-circle"></i>your stripe account connected</span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close()!!}
                    </div>
                    <div class="col-sm-4 col-xs-12  panel-part" style="background-color:#292F51">
                        <h1 class="text-left panel-second-half-heading register-panel" style="color:#fff;">What Seller can do?</h1>
                        <p style="color:#fff;">
                            <i class="fa fa-archive" aria-hidden="true"></i> Seller can create online store.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload video or Audio sample.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-comments" aria-hidden="true"></i> Send and receive messages from Buyers.</p>
                        <p style="color:#fff;">
                            <i class="fa fa-video-camera" aria-hidden="true"></i> Sell their products in digital format- pdf, jpeg, videos etc.
                        </p>
                        <p style="color:#fff;">
                            <i class="fa fa-credit-card-alt" aria-hidden="true"></i> Sell for free and receive up to 70% commission from each sell.</p>
                    </div>
                </div>
                <!-- second tab closed -->
            </div>
        </div>
    <div class="col-sm-2"></div>
</div>
@endsection 

