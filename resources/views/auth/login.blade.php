@extends('layouts.talent') @section('content')
<div class="container-fluid" style="height:275px;background-image:url('assets/images/header-bg.jpg');background-size:cover;">
</div>
<div class="container register-container">
    <div class="col-sm-8 col-md-12 col-xs-12">
        <div class="col-sm-8  col-md-6 col-xs-12 panel-part" style="background-color:#151829;">
            <h3 class="register-panel text-center mb-2" style="color:#fff;"> Login </h3>
             {!! Form::open(['route' => 'login']) !!} @csrf
            <input type="hidden" name="role_id" value="3">
                <div class="input-group" style="margin-bottom:8px;">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    {!! Form::text('email', old('email') , ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'User name OR Email' ]) !!}
                    {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                    <input type="password" class="form-control" name="password" required placeholder="Password">
                    {!! $errors->first('password', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <div class="row">
                    <div class="col-sm-7 col-md-7 col-xs-7 no-padding-right">
                        <div class="fom-inline">
                            <div class="checkbox">
                                <label style="font-size:12px;">
                                    {!! Form::checkbox('remember', old('remember') , ['class' => 'form-check-input' ]) !!} Remember Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5 col-xs-5 no-padding">
                        <div class="fom-inline">
                            <div class="checkbox">
                                @if (Route::has('password.request'))
                                <a style="color:#ff503f;margin-left:12px;font-size:12px;" href="{{ route('password.request') }}">
                                    {{ __('Forgot Password?') }}
                                </a> @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                            <button class="btn btn-danger btn-lg" type="submit" name="button">Login</button>
                        
                    </div>
                </div>
            {!! Form::close()!!}
        </div>
        <div class="col-sm-4 col-md-6 col-xs-12 text-center" style="background-image:url('assets/images/news-2.jpg'); height: 369px; background-size:cover;background-position:left;">
        <div class=" login-right-sec">
                <p style="color:#fff;">Welcome to</p>
                <h3 style="color:#fff;">FutureStarr</h3>
                <p style="color:#fff;">The Official Talent Marketplace</p>
            </div>
        </div>
    </div>
</div>
@endsection 

