<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentCatagoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_catagories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('catagory_image_path');
            $table->text('catagory_desc');
            $table->string('catagory_main_banner');
            $table->string('catagory_banner');
            $table->string('catagory_detailed_banner');
            $table->string('catagory_detailed_icon_img');
            $table->string('tarending_catagory_sidebar_icon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent_catagories');
    }
}
