<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('catagory_id')->unsigned();
            $table->foreign('catagory_id')->references('id')->on('talent_catagories')->onDelete('cascade');
            $table->string('title');
            $table->string('description',700)->nullable();
            $table->string('price');
            $table->text('product_info')->nullable();
            $table->integer('avg_rating');
            $table->integer('view');
            $table->string('facebookLink')->nullable();
            $table->string('twitterLink')->nullable();
            $table->string('active')->default('Active');
            $table->tinyInteger('approved')->default(0);
            $table->integer('delete_flag')->default(0);
            $table->date('date')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talents');
    }
}
