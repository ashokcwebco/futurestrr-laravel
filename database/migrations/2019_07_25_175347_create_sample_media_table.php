<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSampleMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreign('talent_id')->references('id')->on('talents')->onDelete('cascade');
            $table->bigInteger('talent_id')->unsigned();
            $table->string('video_name');
            $table->string('path_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample_media');
    }
}
