-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 11, 2019 at 03:15 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.1.29-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `futurestrr`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_contents`
--

CREATE TABLE `blog_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_contents`
--

INSERT INTO `blog_contents` (`id`, `cat_id`, `title`, `blog_img`, `content`, `date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'What Makes a Great Author?', 'talent-mall-category-images/blog/great_author.jpg', '<p>Becoming a published author can be a difficult road and understanding what sets great writers apart from mediocre ones is key when you want your writing to stand out against the crowd. If you want to make your work shine, there a few traits every great writer has that allows their work to excel past the competition. If you already have these qualities, then you’re one step closer to the best sellers list, but if you\'re unsure, read on to learn what you should be doing to improve your writing.\n    </p>\n<p>A fantastic writer has the ability to size up any and all content, whether they\'re identifying problems in a speech or knowing exactly where a story fails and why. They also have an instinct for words. Good writers know what will catch attention and how each and every word and sentence flows together to present a story or idea. Remarkable writers are able to connect the dots of every little detail and understand how to bring characters to life, tap into an emotion, and close out a paragraph with lasting meaning.</p>\n<p>Writing is a lot like problem solving and requires the ability to clearly express ideas. Its one thing to get thesaurus-happy when producing a mass creative piece, but it\'s important to understand how to translate big words and ideas into a format that nearly anyone can comprehend.\n    </p>\n<p>If you think you’ve got what it takes to be a famous author, playwright, poet, or any other type of writer, visit FutureStarr.com today and sign up for access to our talent mall and Starr search features. Through\n        <font color="#0000ff"><u><a href="https://www.futurestarr.com/starr-search/">Starr\nsearch</a></u></font>, you’ll be able to read other work from aspiring authors like you and even scope out the competition to see what you might be doing right or wrong.</p>', '2018-04-11', 1, 1, '2018-04-11 03:51:23', '2018-04-11 03:51:23'),
(2, 4, 'How to Get Your Foot in the Door of the Music Industry?', 'talent-mall-category-images/blog/dosoftelentmanage2cc8c171.jpg', '<p>Whether you want to be the next big rock star or the one behind the scenes, getting a head start in the music industry isn\'t easy. Very few industries are more exciting to work in and though it\'s not all fame and fortune, a music career can be extremely rewarding for those fit for the job. When you\'re looking to get your foot in the door and sell your talent to record labels, there are a few things you can do that won\'t necessarily guarantee you success, but will put you well on your way towards it.</p>\n<ul>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Be\n	specific with what you want.</b> If music is your passion and you\n	just want to be working with it in any way possible, that\'s great.\n	However, choosing the career you want within the industry will allow\n	you to not only pin-point your own personal goals, but to market\n	yourself to potential employers and talent hunters. </span></span>\n            </font>\n            </font>\n            </font\'\n        </p>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Invest\n	in education and internships.</b> Whether you\'re self-taught or\n	had been taking classes for years, doing what you can to further\n	your musical education can go a long way in moving you closer to\n	your goal. There are a variety of music-related programs available\n	and these can also lead you into valuable internships that can get\n	you in the door of the company or label you want to work for in the\n	future.</span></span>\n            </font>\n            </font>\n            </font>\n        </p>\n        <li/>\n        <p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n            <span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000"><b>Join\n	a talent and recruitment agency.</b> At FutureStarr.com, we provide\n	a variety of different resources for talents in all creative\n	industries. You can <font color="#0000ff"><u><a href="https://www.futurestarr.com/talent-mall/">sell\n	your talent</a></u></font> through our talent mall, where users and\n	potential employers can view and purchase your work. Sign up for\n	Future Starr today and get on the path to a bright musical career!</span></span>\n            </font>\n            </font>\n            </font>\n        </p>\n    </ul>', '2018-04-11', 1, 1, '2018-04-11 03:51:23', '2018-04-11 03:51:23'),
(3, 7, 'So you want to be America’s Next Male Model?', 'talent-mall-category-images/blog/America_Next_Male_Model.jpg', '<p>A career in modeling\nrequires a lot of hard work and dedication and is just as difficult\nfor men as it can be for women. If you think you have what it takes\nto become a male model you have a long road ahead, but with some\npatience and determination you could very well find yourself in the\nfashion world someday.</p>\n<p >One of the first and\nmost important steps is to gain exposure. You can do this by finding\nand submitting your photos to <font color="#0000ff"><u><a href="https://www.futurestarr.com/modeling/">male\nmodeling agencies</a></u></font> such as FutureStarr.com. Just like\nwith women, there are industry standards that most men are expected\nto meet in order to receive work in the modeling industry.</p>\n<ul>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Industry\n	standards require men to be between 5\'11” and 6\'2”.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">The\n	“young men\'s” market requires models aged from 15 to 25.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">The\n	“adult men\'s” market then requires models aged 25 to 35.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Average\n	weight for men is expected to be between 140 and 165 pounds, but can\n	vary depending on you BMI.</span></span></font></font></font></p>\n	<li/>\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; line-height: 115%">\n	<span style="display: inline-block; border: none; padding: 0in"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><span style="background: #ffffff"><font color="#000000">Average\n	measurements are 40 regular to 42 long.</span></span></font></font></font></p>\n</ul>\n<p style="margin-bottom: 0in; line-height: 115%">At FutureStarr.com,\nwe give you the opportunity to promote yourself through our Talent\nMall to get yourself in front of potential employers and other male\nmodeling agencies. If you think you\'ve got what it takes to be the\nhottest new male model, sign up for an account with us today!</p>', '2018-08-28', 1, 1, '2018-08-28 03:00:06', '2018-08-28 03:00:06'),
(4, 1, 'The\r\n“Dos” of Talent Management', 'talent-mall-category-images/blog/talent_management.jpg', '<p style="margin-bottom: 0in; line-height: 115%">When you\'re in the\nbusiness of <font color="#0000ff"><u><a href="http://futurestarr.com/future_star/contact-us">talent\nmanagement</a></u></font>, it\'s important to make sure you\'re\nutilizing all of your resources and skills to not only keep business\nafloat, but to keep your talent happy as well. Below are 3 tips on\nhow to run a successful agency.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>1.\nCreate the Right Look</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">It\'s important\nthat the talent and recruitment facing appearance doesn\'t only fit\nwith the industry you\'re marketing towards, but also shows that\nyour company produces results. By featuring jobs and talent\nprogressions through social media and other aspects of your business,\nyou\'ll prove that you\'re what the client is looking for and that\nyou\'ll help them get to where they want to be.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>2.\nNever Stop Investing in Your Talent and Staff</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">By executing\ndevelopment plans, learning materials (digital or physical), and\ntraining teams or courses can help keep your employees and talent\ninvolved within the company so they don\'t feel that they\'re on\ntheir own in terms of promotion and getting the skills and assistance\nthey may need.</p>\n<p align="left" style="margin-bottom: 0.14in; line-height: 0.2in"><b>3.\nIdentify Potential Early On</b></p>\n<p style="margin-bottom: 0in; line-height: 115%">By identifying what\npeople want when they join your agency right away, you can work to\nhelp them progress through your talent management while still meeting\nbusiness objectives. Recruitment is only the first step for your\ncompany and in order to maintain your client base and bring in new\ntalent, you\'ll need to do everything possible to help each\nindividual succeed.</p>', '2018-08-28', 1, 1, '2018-08-28 03:01:32', '2018-08-28 03:01:32'),
(5, 16, 'Tattoo\r\nArtists: What Happens When You Choose this Awesome Career Path', 'talent-mall-category-images/blog/tatoo_artist_290820180101.jpg', '<p>Becoming a\nprofessional tattoo artist demands a lot of time, talent, and\ntenacity. Well-known and sought-after experts in this field go\nthrough numerous hurdles before they achieve their goal. The good\nnews is that there are tried and true methods on how you can become a\nrespected and certified body arts specialist. \n</p>\n\n<p ><b>Surefire\nWays to Become a Professional Tattoo Artist</b></p>\n\n<p >Learning the trade\nand continuously improving your craft as a tattoo artist is an\nongoing process. There are new trends and techniques about body arts\nthat debut in its niche every now and then. \n</p>\n\n<p >Here are some of the\nimportant ways to help you have a solid stronghold for a successful\ncareer:</p>\n\n<p ><b>Understand\nyour Artwork</b></p>\n\n<p >Tattoos are not just\nabout inking the body with graphics and images. As a certified tattoo\nartist, you need to know the heart and soul of your artwork.\nTattooing is a craft and not just superficial skin arts. The best way\nto establish your career as a tattoo artist is to imbibe a passionate\ndrive in learning tattoos from their origins to the latest trends. \n</p>\n\n<p ><b>Be\nVersatile in your Talent</b></p>\n\n<p >Body art is a\ndynamic and ever-changing field. Thus, you need to maintain an open\nmind if you want better opportunities, new learning, and more\nprofound ideas. You can focus on one particular genre but make sure\nyou also know the other facets of body arts. Talented and flexible\ntattoo artists specialize in various fields from Celtic to Asian to\ntribal and so much more.</p>\n\n<p ><b>Persevere\nand Persevere Some More</b></p>\n\n<p >Learning the ropes\nof the trade is different from mastery of the trade. Tattooing\nrequires hard work, dedication, and perseverance. Even if you have\nthe raw materials and talents, you need to learn, train, and perfect\nyour craft. The adage “Practice Makes Perfect” is highly\napplicable in this particular field. Make sure you are also open to\nnew trends and more advanced techniques to cope with the demands of\nyour customer base. \n</p>\n\n<p ><b>Become\nan Apprentice</b></p>\n\n<p >Every master tattoo\nartist starts with becoming a student in their trade. Thus,\napprenticeship is a vital part of your journey to become the best in\nyour craft. Find established artists that will take you on as an\napprentice. Apprenticeship usually takes place for a specific period\nof time that you agreed upon. It takes a lot of observation and\npractice before an aspiring tattoo artist is allowed to use the\nmachine and ink living skin.</p>\n\n<p ><b>Use\nTalent Marketing Sites</b></p>\n\n<p >Once you are fully\nready to jump start your career, it is high time to find your\ncustomer base. Nowadays, it is extra challenging to compete with\nother tattoo artists. You can have an edge if you use online talent\nmarketing sites to promote your services, prosper online, and\nestablish your clientele. \n</p>\n\n<p >The art of tattooing\nis becoming more than just a form of body art. More and more people\ndiscover the essence of tattoos and the beauty of self-expression\nwith it. Begin your journey as a certified tattoo artist today!</p>', '2018-08-29', 1, 1, '2018-08-29 02:39:36', '2018-08-29 02:39:36'),
(6, 2, 'One Thing All Successful Celebrities Have in Common', '', '<p>Do you want to be\nsuccessful? It is not enough to want success, you must be passionate\nabout it, you must be determined and ready to make sacrifices to\nachieve what you want. In this time and age, talent is overrated. In\norder to become successful in your niche, you need to sell your\ntalent online. Your approach must be creative and long-reaching. Show\nprospects that you have the edge against your competitors. Give them\nthe reason why they should pick you among dozens of other qualified\ncandidates. Before you outline your skills to your potential audience\nor employer, it is imperative to know how you can actually reach\nthem. \n</p>\n\n<p >\n<b>Sell your Talent Online with a Trusted Online Talent Marketplace</b></p>\n\n<p >A talent marketplace\nis the best starting point where you can reach your target employer\nor audience. Whatever talent you may have, you can get the services\nyou need in order to promote it and have your exposure. Talent\nmarketing allows you to be discovered. It is a vehicle where you can\noffer fresh ideas and share your talent to your preferred niche.</p>\n\n<p >The Internet is huge\nand free resources where you can jump start your endeavors. You sell\nyour talent online and get immediate results. Opting for talent\nmarketing in the Internet allows you to be more creative and even\nsave time, money, and effort. There\'s a big difference when you\npromote your skills and talents online than personally going to your\ntarget employer. It gives you farther, wider, and longer reach. \n</p>\n\n<p >\n<b>Helpful Pointers for Budding Talents</b></p>\n\n<p ><span style="background: #ffffff"><font color="#000000"><span style="background: #ffffff">Once\nyou have found the right venue where you can </span></font>sell your\ntalent online, it is about time you work on your promotional and\nmarketing schemes. Here are some practical and proven tips:</span></p>\n\n<ul>\n	<li/>\n<p ><font color="#000000"><b>Find\n	and Focus on Your Niche</b></font></p>\n</ul>\n\n<p ><font color="#000000">Identifying\nyour niche definitely works. The key is for you to have a solid and\ncoherent plan that focuses on the target audience or market. Your\nniche is your forte, somewhere your talent is most applicable and\nwhere you can work on your success in the future.</font></p>\n\n<ul>\n	<li/>\n<p ><font color="#000000"><b>Start\n	and Keep Going</b></font></p>\n</ul>\n\n<p ><span style="background: #ffffff"><font color="#000000"><span style="background: #ffffff">It\nis not enough that you </span></font>sell your talent online. Start\nyour talent marketing venture and never give up because there will\nalways be hurdles along the way. Stick with your plan, nurture it,\nand keep going especially when you encounter failures.</span></p>\n\n<p ><span style="background: #ffffff">The\nfirst step to realizing your dreams is to sell your talent online.\nThe good news is that you have numerous tools and resources to use\nfor endorsing your skills, whatever they may be. Find a trusted and\nreputable talent agency now!</span></p>', '2018-08-29', 1, 1, '2018-08-29 02:40:10', '2018-08-29 02:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `commercial_media`
--

CREATE TABLE `commercial_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `talent_id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commercial_media`
--

INSERT INTO `commercial_media` (`id`, `talent_id`, `image_name`, `image_path`, `created_at`, `updated_at`) VALUES
(64, 170, 'images.png', 'uploads/commercial-product/170-images.png', '2018-05-01 03:07:59', '2018-05-01 03:07:59'),
(65, 171, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-01 03:16:12', '2018-05-01 03:16:12'),
(66, 172, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-01 03:28:22', '2018-05-01 03:28:22'),
(67, 173, 'author.jpg', 'uploads/commercial-product/173-author.jpg', '2018-05-01 05:41:12', '2018-05-01 05:41:12'),
(68, 174, 'home-page.PNG', 'uploads/commercial-product/170-home-page.PNG', '2018-05-02 00:14:39', '2018-05-02 00:14:39'),
(69, 175, 'NEw site 2.PNG', 'uploads/commercial-product/175-NEw site 2.PNG', '2018-05-14 14:40:38', '2018-05-14 14:40:38'),
(70, 176, 'food.jpg', 'uploads/commercial-product/193-food.jpg', '2018-05-14 22:53:32', '2018-05-14 22:53:32'),
(72, 178, '8.jpg', 'uploads/commercial-product/178-8.jpg', '2018-07-09 00:31:26', '2018-07-09 00:31:26'),
(74, 180, 'seller.jpg', 'uploads/commercial-product/180-seller.jpg', '2018-08-02 05:52:03', '2018-08-02 05:52:03'),
(75, 182, 'Happy_Birthday.jpg', 'uploads/commercial-product/182-Happy_Birthday.jpg', '2018-08-06 08:09:28', '2018-08-06 08:09:28'),
(76, 184, '2000px-Face-smile.svg.png', 'uploads/commercial-product/184-2000px-Face-smile.svg.png', '2018-08-09 03:53:44', '2018-08-09 03:53:44'),
(80, 185, '2.jpg', 'uploads/commercial-product/185-2.jpg', '2018-08-09 05:13:05', '2018-08-09 05:13:05'),
(81, 186, 'entertainment.jpg', 'uploads/commercial-product/186-entertainment.jpg', '2018-08-10 07:56:46', '2018-08-10 07:56:46'),
(82, 187, 'Music.jpg', 'uploads/commercial-product/187-Music.jpg', '2018-08-10 08:11:51', '2018-08-10 08:11:51'),
(83, 188, 'model.jpg', 'uploads/commercial-product/188-model.jpg', '2018-08-10 08:45:49', '2018-08-10 08:45:49'),
(84, 189, 'Tatoo artist.jpg', 'uploads/commercial-product/189-Tatoo artist.jpg', '2018-08-11 04:44:18', '2018-08-11 04:44:18'),
(85, 190, 'Fashion Design.jpg', 'uploads/commercial-product/190-Fashion Design.jpg', '2018-08-11 04:48:56', '2018-08-11 04:48:56'),
(86, 191, 'mathematics.jpg', 'uploads/commercial-product/191-mathematics.jpg', '2018-08-11 05:07:51', '2018-08-11 05:07:51'),
(87, 192, 'Nutrition.jpg', 'uploads/commercial-product/192-Nutrition.jpg', '2018-08-11 07:01:35', '2018-08-11 07:01:35'),
(88, 193, 'food.jpg', 'uploads/commercial-product/193-food.jpg', '2018-08-11 07:02:23', '2018-08-11 07:02:23'),
(89, 194, 'science.png', 'uploads/commercial-product/194-science.png', '2018-08-11 07:03:08', '2018-08-11 07:03:08'),
(90, 195, 'natural geography.jpg', 'uploads/commercial-product/195-natural geography.jpg', '2018-08-11 07:05:20', '2018-08-11 07:05:20'),
(91, 196, 'cosmatic.jpg', 'uploads/commercial-product/196-cosmatic.jpg', '2018-08-11 07:08:58', '2018-08-11 07:08:58'),
(92, 197, 'fitness.jpg', 'uploads/commercial-product/197-fitness.jpg', '2018-08-11 07:12:04', '2018-08-11 07:12:04'),
(93, 198, 'buyer.jpg', 'uploads/commercial-product/198-buyer.jpg', '2018-08-14 00:50:40', '2018-08-14 00:50:40'),
(96, 199, 'science.png', 'uploads/commercial-product/199-science.png', '2018-08-16 05:38:59', '2018-08-16 05:38:59'),
(97, 200, 'science.png', 'uploads/commercial-product/200-science.png', '2018-08-16 05:48:26', '2018-08-16 05:48:26'),
(98, 201, '2.jpg', 'uploads/commercial-product/201-2.jpg', '2018-08-16 05:58:20', '2018-08-16 05:58:20'),
(99, 202, '1.png', 'uploads/commercial-product/202-1.png', '2018-08-16 06:27:11', '2018-08-16 06:27:11'),
(100, 203, 'Author.jpeg', 'uploads/commercial-product/203-Author.jpeg', '2018-08-16 06:28:52', '2018-08-16 06:28:52'),
(101, 204, '1.png', 'uploads/commercial-product/204-1.png', '2018-08-16 07:12:46', '2018-08-16 07:12:46'),
(102, 205, '1.png', 'uploads/commercial-product/205-1.png', '2018-08-16 08:10:19', '2018-08-16 08:10:19'),
(103, 206, '1.png', 'uploads/commercial-product/206-1.png', '2018-08-16 08:13:21', '2018-08-16 08:13:21'),
(104, 207, 'model.jpg', 'uploads/commercial-product/207-model.jpg', '2018-08-17 03:38:25', '2018-08-17 03:38:25'),
(105, 208, 'Music.jpg', 'uploads/commercial-product/208-Music.jpg', '2018-08-17 06:24:18', '2018-08-17 06:24:18'),
(106, 209, 'seller.jpg', 'uploads/commercial-product/209-seller.jpg', '2018-08-20 03:56:47', '2018-08-20 03:56:47'),
(107, 210, 'F8C4682E-D261-4E45-AA47-B30C552786E3.jpeg', 'uploads/commercial-product/210-F8C4682E-D261-4E45-AA47-B30C552786E3.jpeg', '2018-08-20 05:46:57', '2018-08-20 05:46:57'),
(108, 211, 'model.jpg', 'uploads/commercial-product/211-model.jpg', '2018-08-22 01:59:03', '2018-08-22 01:59:03'),
(109, 212, 'mathematics.jpg', 'uploads/commercial-product/212-mathematics.jpg', '2018-08-22 05:54:32', '2018-08-22 05:54:32'),
(110, 213, 'entertainment4.jpg', 'uploads/commercial-product/213-entertainment4.jpg', '2018-08-22 07:58:36', '2018-08-22 07:58:36'),
(111, 220, 'create ads social buzz image.png', 'uploads/commercial-product/220-create ads social buzz image.png', '2018-08-24 06:47:37', '2018-08-24 06:47:37'),
(112, 221, 'National Geography.jpg', 'uploads/commercial-product/221-National Geography.jpg', '2018-08-27 04:16:03', '2018-08-27 04:16:03'),
(113, 222, 'model2.jpg', 'uploads/commercial-product/222-model2.jpg', '2018-08-27 04:23:27', '2018-08-27 04:23:27'),
(114, 223, 'science.png', 'uploads/commercial-product/223-science.png', '2018-08-27 05:50:45', '2018-08-27 05:50:45'),
(115, 224, 'National Geography.jpg', 'uploads/commercial-product/224-National Geography.jpg', '2018-08-28 03:32:58', '2018-08-28 03:32:58'),
(116, 225, 'fitness2.jpg', 'uploads/commercial-product/225-fitness2.jpg', '2018-08-28 05:47:31', '2018-08-28 05:47:31'),
(117, 227, 'music.jpg', 'uploads/commercial-product/227-music.jpg', '2018-08-29 03:52:21', '2018-08-29 03:52:21'),
(118, 228, 'output_html_e66d0afebbada2b.jpg', 'uploads/commercial-product/228-output_html_e66d0afebbada2b.jpg', '2018-08-29 03:52:40', '2018-08-29 03:52:40'),
(119, 229, 'National Geography.jpg', 'uploads/commercial-product/229-National Geography.jpg', '2018-08-29 03:55:40', '2018-08-29 03:55:40'),
(120, 226, 'National Geography.jpg', 'uploads/commercial-product/226-National Geography.jpg', '2018-08-29 03:56:18', '2018-08-29 03:56:18'),
(121, 230, 'entertainment4.jpg', 'uploads/commercial-product/230-entertainment4.jpg', '2018-08-29 04:42:14', '2018-08-29 04:42:14'),
(122, 231, 'science fiction.jpeg', 'uploads/commercial-product/231-science fiction.jpeg', '2018-08-29 04:45:47', '2018-08-29 04:45:47'),
(123, 232, 'lets.png', 'uploads/commercial-product/232-lets.png', '2018-08-29 04:49:12', '2018-08-29 04:49:12'),
(124, 233, 'fb.png', 'uploads/commercial-product/233-fb.png', '2018-08-29 04:53:36', '2018-08-29 04:53:36'),
(125, 234, 'user-profile.png', 'uploads/commercial-product/234-user-profile.png', '2018-08-29 04:55:36', '2018-08-29 04:55:36'),
(126, 235, 'music.jpg', 'uploads/commercial-product/235-music.jpg', '2018-08-29 05:05:11', '2018-08-29 05:05:11'),
(127, 236, 'photography.jpg', 'uploads/commercial-product/236-photography.jpg', '2018-08-29 05:07:48', '2018-08-29 05:07:48'),
(128, 237, 'fitness.jpg', 'uploads/commercial-product/237-fitness.jpg', '2018-08-29 05:41:18', '2018-08-29 05:41:18'),
(129, 238, 'user-profile.png', 'uploads/commercial-product/238-user-profile.png', '2018-08-29 05:42:42', '2018-08-29 05:42:42'),
(130, 239, 'cosmatics1.png', 'uploads/commercial-product/239-cosmatics1.png', '2018-08-30 08:21:14', '2018-08-30 08:21:14'),
(131, 240, 'fitness.jpg', 'uploads/commercial-product/240-fitness.jpg', '2018-08-31 04:06:38', '2018-08-31 04:06:38'),
(132, 241, 'img1.jpeg', 'uploads/commercial-product/241-img1.jpeg', '2018-08-31 04:55:07', '2018-08-31 04:55:07'),
(133, 242, 'National Geography.jpg', 'uploads/commercial-product/242-National Geography.jpg', '2018-08-31 06:11:09', '2018-08-31 06:11:09'),
(134, 243, 'seller.jpg', 'uploads/commercial-product/243-seller.jpg', '2018-09-03 00:32:00', '2018-09-03 00:32:00'),
(141, 244, '555624c5-70fa-4560-99a7-e47ec7b412d0.jpg', 'uploads/commercial-product/244-555624c5-70fa-4560-99a7-e47ec7b412d0.jpg', '2018-09-03 00:55:32', '2018-09-03 00:55:32'),
(142, 245, 'photo.jpeg', 'uploads/commercial-product/245-photo.jpeg', '2018-09-03 03:21:47', '2018-09-03 03:21:47'),
(143, 246, 'mathematics3.jpg', 'uploads/commercial-product/246-mathematics3.jpg', '2018-09-03 05:47:08', '2018-09-03 05:47:08'),
(144, 247, 'nutrition.jpg', 'uploads/commercial-product/247-nutrition.jpg', '2018-09-03 06:04:19', '2018-09-03 06:04:19'),
(145, 248, 'national geo.png', 'uploads/commercial-product/248-national geo.png', '2018-09-04 03:50:55', '2018-09-04 03:50:55'),
(146, 249, 'seller.jpg', 'uploads/commercial-product/249-seller.jpg', '2018-09-07 06:15:06', '2018-09-07 06:15:06'),
(147, 218, 'entertainment.jpg', 'uploads/commercial-product/218-entertainment.jpg', '2018-09-07 06:27:20', '2018-09-07 06:27:20'),
(148, 219, 'fashion designer.jpg', 'uploads/commercial-product/219-fashion designer.jpg', '2018-09-07 06:29:35', '2018-09-07 06:29:35'),
(149, 216, 'food.jpg', 'uploads/commercial-product/216-food.jpg', '2018-09-07 06:30:47', '2018-09-07 06:30:47'),
(150, 217, 'comedy 1.jpeg', 'uploads/commercial-product/217-comedy 1.jpeg', '2018-09-07 06:34:06', '2018-09-07 06:34:06'),
(151, 250, 'Fs.PNG', 'uploads/commercial-product/250-Fs.PNG', '2018-09-08 21:35:11', '2018-09-08 21:35:11'),
(152, 252, 'SampleVideo_1280x720_5mb.mkv', 'uploads/commercial-product/252-SampleVideo_1280x720_5mb.mkv', '2018-09-10 00:07:45', '2018-09-10 00:07:45'),
(153, 253, 'SampleVideo_1280x720_5mb.mkv', 'uploads/commercial-product/253-SampleVideo_1280x720_5mb.mkv', '2018-09-10 00:12:58', '2018-09-10 00:12:58'),
(154, 256, 'SampleVideo_360x240_30mb.mp4', 'uploads/commercial-product/256-SampleVideo_360x240_30mb.mp4', '2018-09-10 00:31:32', '2018-09-10 00:31:32'),
(155, 257, 'model.jpg', 'uploads/commercial-product/257-model.jpg', '2018-09-10 00:43:57', '2018-09-10 00:43:57'),
(156, 254, 'fitness.jpg', 'uploads/commercial-product/254-fitness.jpg', '2018-09-10 01:43:43', '2018-09-10 01:43:43'),
(157, 258, 'Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 'uploads/commercial-product/258-Learn the secrets to making a fortune from your own ( Home Based Business).mp4', '2018-09-10 08:07:09', '2018-09-10 08:07:09'),
(158, 0, '1114907_orig.jpg', 'uploads/commercial-product/[object Object]-1114907_orig.jpg', '2018-09-10 23:48:52', '2018-09-10 23:48:52'),
(159, 259, 'Mov.mov', 'uploads/commercial-product/259-Mov.mov', '2018-09-11 01:09:12', '2018-09-11 01:09:12'),
(160, 260, 'Mov.mov', 'uploads/commercial-product/260-Mov.mov', '2018-09-11 06:48:19', '2018-09-11 06:48:19'),
(161, 261, 'demo.jpg', 'uploads/commercial-product/261-demo.jpg', '2018-09-12 06:17:36', '2018-09-12 06:17:36'),
(162, 262, 'flv.flv', 'uploads/commercial-product/262-flv.flv', '2018-09-14 01:19:48', '2018-09-14 01:19:48'),
(163, 263, 'Mov.mov', 'uploads/commercial-product/263-Mov.mov', '2018-09-14 03:24:15', '2018-09-14 03:24:15'),
(164, 264, 'Capture.JPG', 'uploads/commercial-product/264-Capture.JPG', '2018-10-11 03:58:24', '2018-10-11 03:58:24'),
(165, 265, 'buyer-banner.jpg', 'uploads/commercial-product/265-buyer-banner.jpg', '2018-10-13 06:54:39', '2018-10-13 06:54:39'),
(166, 266, 'ext.jpeg', 'uploads/commercial-product/266-ext.jpeg', '2018-10-18 08:05:35', '2018-10-18 08:05:35'),
(167, 267, 'stardom.png', 'uploads/commercial-product/267-stardom.png', '2018-10-22 07:30:57', '2018-10-22 07:30:57'),
(168, 268, 'buyer-banner.jpg', 'uploads/commercial-product/268-buyer-banner.jpg', '2018-10-22 07:51:34', '2018-10-22 07:51:34'),
(169, 269, 'Screenshot_2018-10-15-12-28-30-646_com.android.chrome.png', 'uploads/commercial-product/269-Screenshot_2018-10-15-12-28-30-646_com.android.chrome.png', '2018-11-11 04:34:53', '2018-11-11 04:34:53'),
(170, 270, 'Learn the secrets to making a fortune from your own ( Home Based Business).mp4', 'uploads/commercial-product/270-Learn the secrets to making a fortune from your own ( Home Based Business).mp4', '2018-11-11 18:38:55', '2018-11-11 18:38:55'),
(171, 271, 'Fs.PNG', 'uploads/commercial-product/271-Fs.PNG', '2018-11-11 18:42:54', '2018-11-11 18:42:54'),
(172, 272, 'banner.jpg', 'uploads/commercial-product/272-banner.jpg', '2018-11-14 08:08:42', '2018-11-14 08:08:42'),
(173, 273, 'imglogo-1539846524854.png', 'uploads/commercial-product/273-imglogo-1539846524854.png', '2018-11-14 08:55:36', '2018-11-14 08:55:36'),
(174, 274, 'SampleVideo_1280x720_2mb.mp4', 'uploads/commercial-product/274-SampleVideo_1280x720_2mb.mp4', '2019-03-26 01:09:26', '2019-03-26 01:09:26'),
(175, 275, 'pumpkin-1768857__340.jpg', 'uploads/commercial-product/275-pumpkin-1768857__340.jpg', '2019-03-26 01:35:51', '2019-03-26 01:35:51'),
(176, 276, 'bread-2796393__340.jpg', 'uploads/commercial-product/276-bread-2796393__340.jpg', '2019-03-26 01:59:58', '2019-03-26 01:59:58'),
(177, 277, 'orange-1995056__340.jpg', 'uploads/commercial-product/277-orange-1995056__340.jpg', '2019-03-26 03:15:55', '2019-03-26 03:15:55'),
(178, 278, 'images (1).jpg', 'uploads/commercial-product/278-images (1).jpg', '2019-03-26 04:09:16', '2019-03-26 04:09:16'),
(179, 279, 'download (1).png', 'uploads/commercial-product/279-download (1).png', '2019-03-26 04:11:18', '2019-03-26 04:11:18'),
(180, 280, 'pumpkin-1768857__340.jpg', 'uploads/commercial-product/280-pumpkin-1768857__340.jpg', '2019-03-26 04:33:20', '2019-03-26 04:33:20'),
(181, 281, 'images (1).jpg', 'uploads/commercial-product/281-images (1).jpg', '2019-03-26 04:55:59', '2019-03-26 04:55:59'),
(182, 282, 'images (2).jpg', 'uploads/commercial-product/282-images (2).jpg', '2019-03-26 05:56:02', '2019-03-26 05:56:02'),
(183, 284, 'salad-2068220__340.jpg', 'uploads/commercial-product/284-salad-2068220__340.jpg', '2019-03-26 06:02:27', '2019-03-26 06:02:27'),
(184, 285, 'input-onlinejpgtools.jpg', 'uploads/commercial-product/285-input-onlinejpgtools.jpg', '2019-04-09 04:30:33', '2019-04-09 04:30:33'),
(185, 286, 'Wildlife-1.jpg', 'uploads/commercial-product/286-Wildlife-1.jpg', '2019-04-10 12:33:55', '2019-04-10 12:33:55'),
(186, 287, 'Biguine-Make-Up-13-1024x767.jpg', 'uploads/commercial-product/287-Biguine-Make-Up-13-1024x767.jpg', '2019-04-21 06:56:04', '2019-04-21 06:56:04'),
(187, 288, 'download.jpeg', 'uploads/commercial-product/288-download.jpeg', '2019-04-21 13:35:16', '2019-04-21 13:35:16'),
(188, 289, 'CL-MS-02_ml.jpg', 'uploads/commercial-product/289-CL-MS-02_ml.jpg', '2019-04-21 14:13:34', '2019-04-21 14:13:34'),
(189, 290, 'CL-MS-08-V2_ml.jpg', 'uploads/commercial-product/290-CL-MS-08-V2_ml.jpg', '2019-04-21 14:27:11', '2019-04-21 14:27:11'),
(190, 291, 'videoplayback.mp4', 'uploads/commercial-product/291-videoplayback.mp4', '2019-04-23 11:48:11', '2019-04-23 11:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_1000000_create_users_roles_table', 1),
(12, '2014_10_12_1000000_create_users_table', 1),
(13, '2014_10_12_2000000_create_password_resets_table', 1),
(14, '2019_07_07_054846_create_talent_catagories_table', 1),
(15, '2019_07_07_063642_create_blog_contents_table', 1),
(16, '2019_07_17_080318_create_talents_table', 2),
(17, '2019_07_20_030403_create_commercial_media_table', 3),
(18, '2019_07_25_175347_create_sample_media_table', 4),
(19, '2019_08_04_115223_create_plans_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` mediumint(9) NOT NULL,
  `period` mediumint(9) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `title`, `description`, `price`, `period`, `created_at`, `updated_at`) VALUES
(1, 'Basic Plan', '10 clicks per month', 20, 30, '2017-12-07 07:34:55', '2017-12-07 07:34:55'),
(2, 'Economy Plan', '25 clicks per month', 40, 30, '2017-12-07 07:34:55', '2017-12-07 07:34:55'),
(3, 'Ultimate Plan', '100 clicks per month', 175, 30, '2017-12-07 07:35:25', '2017-12-07 07:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `sample_media`
--

CREATE TABLE `sample_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `talent_id` bigint(20) UNSIGNED NOT NULL,
  `video_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sample_media`
--

INSERT INTO `sample_media` (`id`, `talent_id`, `video_name`, `path_name`, `created_at`, `updated_at`) VALUES
(1, 180, 'step.mov', 'uploads/seller-video/180-step.mov', '2018-08-02 11:22:03', '2018-08-02 11:22:03'),
(2, 182, 'mov_bbb.mp4', 'uploads/seller-video/182-mov_bbb.mp4', '2018-08-06 08:09:01', '2018-08-06 08:09:01'),
(3, 182, 's9qgz-g5flo.mp4', 'uploads/seller-video/182-s9qgz-g5flo.mp4', '2018-08-06 08:11:51', '2018-08-06 08:11:51'),
(4, 184, '15MG.mp4', 'uploads/seller-video/184-15MG.mp4', '2018-08-09 03:53:53', '2018-08-09 03:53:53'),
(5, 185, '15MG.mp4', 'uploads/seller-video/185-15MG.mp4', '2018-08-09 05:13:53', '2018-08-09 05:13:53'),
(6, 186, '15MG.mp4', 'uploads/seller-video/186-15MG.mp4', '2018-08-10 07:56:49', '2018-08-10 07:56:49'),
(7, 187, '15MG.mp4', 'uploads/seller-video/187-15MG.mp4', '2018-08-10 08:11:55', '2018-08-10 08:11:55'),
(8, 188, '15MG.mp4', 'uploads/seller-video/188-15MG.mp4', '2018-08-10 08:45:56', '2018-08-10 08:45:56'),
(9, 189, 'cat 30 sec.mp4', 'uploads/seller-video/189-cat 30 sec.mp4', '2018-08-11 04:44:13', '2018-08-11 04:44:13'),
(10, 190, 'cat 30 sec.mp4', 'uploads/seller-video/190-cat 30 sec.mp4', '2018-08-11 04:49:16', '2018-08-11 04:49:16'),
(11, 191, 'animation.mp4', 'uploads/seller-video/191-animation.mp4', '2018-08-11 05:08:11', '2018-08-11 05:08:11'),
(12, 192, 'animation.mp4', 'uploads/seller-video/192-animation.mp4', '2018-08-11 07:01:50', '2018-08-11 07:01:50'),
(13, 193, 'cat 30 sec.mp4', 'uploads/seller-video/193-cat 30 sec.mp4', '2018-08-11 07:02:34', '2018-08-11 07:02:34'),
(14, 194, 'cat 30 sec.mp4', 'uploads/seller-video/194-cat 30 sec.mp4', '2018-08-11 07:03:11', '2018-08-11 07:03:11'),
(15, 195, '15MG.mp4', 'uploads/seller-video/195-15MG.mp4', '2018-08-11 07:05:24', '2018-08-11 07:05:24'),
(16, 196, '15MG.mp4', 'uploads/seller-video/196-15MG.mp4', '2018-08-11 07:09:09', '2018-08-11 07:09:09'),
(17, 197, 'cat 30 sec.mp4', 'uploads/seller-video/197-cat 30 sec.mp4', '2018-08-11 07:12:08', '2018-08-11 07:12:08'),
(18, 198, 'cat 30 sec.mp4', 'uploads/seller-video/198-cat 30 sec.mp4', '2018-08-14 00:50:46', '2018-08-14 00:50:46'),
(19, 199, 'cat 30 sec.mp4', 'uploads/seller-video/199-cat 30 sec.mp4', '2018-08-16 05:39:07', '2018-08-16 05:39:07'),
(20, 200, '15MG.mp4', 'uploads/seller-video/200-15MG.mp4', '2018-08-16 05:49:13', '2018-08-16 05:49:13'),
(21, 202, '15MG.mp4', 'uploads/seller-video/202-15MG.mp4', '2018-08-16 06:27:28', '2018-08-16 06:27:28'),
(22, 203, 'animation.mp4', 'uploads/seller-video/203-animation.mp4', '2018-08-16 06:29:14', '2018-08-16 06:29:14'),
(23, 204, 'animation.mp4', 'uploads/seller-video/204-animation.mp4', '2018-08-16 07:13:05', '2018-08-16 07:13:05'),
(24, 205, '15MG.mp4', 'uploads/seller-video/205-15MG.mp4', '2018-08-16 08:10:51', '2018-08-16 08:10:51'),
(25, 206, '15MG.mp4', 'uploads/seller-video/206-15MG.mp4', '2018-08-16 08:13:30', '2018-08-16 08:13:30'),
(26, 207, '15MG.mp4', 'uploads/seller-video/207-15MG.mp4', '2018-08-17 03:38:33', '2018-08-17 03:38:33'),
(27, 208, '15MG.mp4', 'uploads/seller-video/208-15MG.mp4', '2018-08-17 06:24:26', '2018-08-17 06:24:26'),
(28, 209, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/209-182-s9qgz-g5flo.mp4', '2018-08-22 01:57:18', '2018-08-22 01:57:18'),
(29, 212, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/212-182-s9qgz-g5flo.mp4', '2018-08-22 05:55:20', '2018-08-22 05:55:20'),
(30, 222, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/222-182-s9qgz-g5flo.mp4', '2018-08-27 04:24:34', '2018-08-27 04:24:34'),
(31, 224, '55714760752__58E58D5B-1B83-4477-8DBA-730482DEEE9F.MOV', 'uploads/seller-video/224-55714760752__58E58D5B-1B83-4477-8DBA-730482DEEE9F.MOV', '2018-08-28 05:44:21', '2018-08-28 05:44:21'),
(32, 225, '55714772948__73FB8023-8987-47B0-8312-FA4FBBD22EAB.MOV', 'uploads/seller-video/225-55714772948__73FB8023-8987-47B0-8312-FA4FBBD22EAB.MOV', '2018-08-28 05:47:21', '2018-08-28 05:47:21'),
(33, 230, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/230-SampleVideo_1280x720_5mb.mkv', '2018-08-29 04:42:40', '2018-08-29 04:42:40'),
(34, 231, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/231-SampleVideo_1280x720_5mb.mkv', '2018-08-29 04:46:03', '2018-08-29 04:46:03'),
(35, 232, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/232-SampleVideo_1280x720_5mb.mkv', '2018-08-29 04:49:26', '2018-08-29 04:49:26'),
(36, 233, 'step.mov', 'uploads/seller-video/233-step.mov', '2018-08-29 04:53:42', '2018-08-29 04:53:42'),
(37, 234, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/234-SampleVideo_1280x720_5mb.mkv', '2018-08-29 04:55:52', '2018-08-29 04:55:52'),
(38, 235, '202-file_example_MP3_700KB.mp3', 'uploads/seller-video/235-202-file_example_MP3_700KB.mp3', '2018-08-29 05:05:26', '2018-08-29 05:05:26'),
(39, 236, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/236-SampleVideo_1280x720_5mb.mkv', '2018-08-29 05:08:14', '2018-08-29 05:08:14'),
(40, 181, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/181-SampleVideo_1280x720_5mb.mkv', '2018-08-29 05:25:59', '2018-08-29 05:25:59'),
(41, 183, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/183-SampleVideo_1280x720_5mb.mkv', '2018-08-29 05:32:11', '2018-08-29 05:32:11'),
(42, 237, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/237-SampleVideo_1280x720_5mb.mkv', '2018-08-29 05:41:35', '2018-08-29 05:41:35'),
(43, 238, 'step.mov', 'uploads/seller-video/238-step.mov', '2018-08-29 05:42:45', '2018-08-29 05:42:45'),
(44, 240, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/240-SampleAudio_0.4mb.mp3', '2018-08-31 04:06:45', '2018-08-31 04:06:45'),
(45, 242, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/242-182-s9qgz-g5flo.mp4', '2018-08-31 06:11:57', '2018-08-31 06:11:57'),
(46, 243, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/243-182-s9qgz-g5flo.mp4', '2018-09-03 00:32:51', '2018-09-03 00:32:51'),
(47, 244, 'ogv.ogv', 'uploads/seller-video/244-ogv.ogv', '2018-09-03 00:55:30', '2018-09-03 00:55:30'),
(48, 245, '202-file_example_MP3_700KB.mp3', 'uploads/seller-video/245-202-file_example_MP3_700KB.mp3', '2018-09-03 03:22:08', '2018-09-03 03:22:08'),
(49, 246, 'video2.mp4', 'uploads/seller-video/246-video2.mp4', '2018-09-03 05:47:09', '2018-09-03 05:47:09'),
(50, 249, 'SampleAudio_0.7mb.mp3', 'uploads/seller-video/249-SampleAudio_0.7mb.mp3', '2018-09-07 06:15:14', '2018-09-07 06:15:14'),
(51, 248, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/248-182-s9qgz-g5flo.mp4', '2018-09-08 04:07:22', '2018-09-08 04:07:22'),
(52, 223, '182-s9qgz-g5flo.mp4', 'uploads/seller-video/223-182-s9qgz-g5flo.mp4', '2018-09-08 04:22:22', '2018-09-08 04:22:22'),
(53, 218, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/218-SampleAudio_0.4mb.mp3', '2018-09-08 04:47:17', '2018-09-08 04:47:17'),
(54, 250, 'Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', 'uploads/seller-video/250-Where promote your new song or talent- - Go To www.FutureStarr.com ---- FutureStarr----.mp4', '2018-09-08 21:35:12', '2018-09-08 21:35:12'),
(55, 256, 'SampleVideo_1280x720_5mb.mkv', 'uploads/seller-video/256-SampleVideo_1280x720_5mb.mkv', '2018-09-10 01:39:38', '2018-09-10 01:39:38'),
(56, 254, 'Mov.mov', 'uploads/seller-video/254-Mov.mov', '2018-09-10 01:44:53', '2018-09-10 01:44:53'),
(57, 213, 'flv.flv', 'uploads/seller-video/213-flv.flv', '2018-09-10 03:30:45', '2018-09-10 03:30:45'),
(58, 257, 'flv.flv', 'uploads/seller-video/257-flv.flv', '2018-09-10 05:06:54', '2018-09-10 05:06:54'),
(59, 259, 'mpeg.mp4', 'uploads/seller-video/259-mpeg.mp4', '2018-09-11 01:09:08', '2018-09-11 01:09:08'),
(60, 260, 'mpeg.mp4', 'uploads/seller-video/260-mpeg.mp4', '2018-09-11 06:48:15', '2018-09-11 06:48:15'),
(61, 261, 'Mov.mov', 'uploads/seller-video/261-Mov.mov', '2018-09-12 06:17:48', '2018-09-12 06:17:48'),
(62, 262, 'Mov.mov', 'uploads/seller-video/262-Mov.mov', '2018-09-14 01:20:03', '2018-09-14 01:20:03'),
(63, 263, 'Ranbuild22.mov', 'uploads/seller-video/263-Ranbuild22.mov', '2018-09-14 03:27:19', '2018-09-14 03:27:19'),
(64, 271, 'ARTIC-TOP-SHELF-MASTER.mp3', 'uploads/seller-video/271-ARTIC-TOP-SHELF-MASTER.mp3', '2018-11-11 18:42:59', '2018-11-11 18:42:59'),
(65, 274, 'SampleAudio_0.4mb.mp3', 'uploads/seller-video/274-SampleAudio_0.4mb.mp3', '2019-03-26 01:09:15', '2019-03-26 01:09:15'),
(66, 275, 'SampleVideo_1280x720_5mb.mp4', 'uploads/seller-video/275-SampleVideo_1280x720_5mb.mp4', '2019-03-26 01:36:09', '2019-03-26 01:36:09'),
(67, 276, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/276-SampleVideo_1280x720_2mb.mp4', '2019-03-26 02:00:08', '2019-03-26 02:00:08'),
(68, 277, 'SampleAudio_0.7mb.mp3', 'uploads/seller-video/277-SampleAudio_0.7mb.mp3', '2019-03-26 03:15:59', '2019-03-26 03:15:59'),
(69, 278, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/278-SampleVideo_1280x720_2mb.mp4', '2019-03-26 04:09:23', '2019-03-26 04:09:23'),
(70, 279, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/279-SampleVideo_1280x720_2mb.mp4', '2019-03-26 04:11:26', '2019-03-26 04:11:26'),
(71, 280, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/280-SampleVideo_1280x720_2mb.mp4', '2019-03-26 04:33:33', '2019-03-26 04:33:33'),
(72, 281, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/281-SampleVideo_1280x720_2mb.mp4', '2019-03-26 04:56:09', '2019-03-26 04:56:09'),
(73, 284, 'SampleVideo_1280x720_2mb.mp4', 'uploads/seller-video/284-SampleVideo_1280x720_2mb.mp4', '2019-03-26 06:03:52', '2019-03-26 06:03:52'),
(74, 286, 'hale_bopp_1.mpg', 'uploads/seller-video/286-hale_bopp_1.mpg', '2019-04-10 12:52:34', '2019-04-10 12:52:34'),
(75, 287, 'Smokey Eye Makeup Video Free Download.mp4', 'uploads/seller-video/287-Smokey Eye Makeup Video Free Download.mp4', '2019-04-21 06:56:10', '2019-04-21 06:56:10'),
(76, 291, 'videohive-10668306-super-stylish-model-dancing-and-posing-6 (online-video-cutter.com).mp4', 'uploads/seller-video/291-videohive-10668306-super-stylish-model-dancing-and-posing-6 (online-video-cutter.com).mp4', '2019-04-23 11:47:48', '2019-04-23 11:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE `talents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `catagory_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(700) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_info` text COLLATE utf8mb4_unicode_ci,
  `avg_rating` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `facebookLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitterLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `instagramLink` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`id`, `user_id`, `catagory_id`, `title`, `description`, `price`, `product_info`, `avg_rating`, `view`, `facebookLink`, `twitterLink`, `active`, `approved`, `delete_flag`, `date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `instagramLink`) VALUES
(170, 1, 1, 'test1', 'welcome1', '454', 'test data1', 3, 16, 'www.facebook.com', NULL, 'Deactive', 0, 0, '2018-05-01', 1, 1, '2018-04-30 19:34:21', '2018-04-30 19:34:21', NULL),
(173, 1, 6, 'dcd', NULL, '33', 'updated/........', 5, 71, NULL, NULL, 'Active', 1, 0, '2018-05-01', 1, 1, '2018-04-30 21:58:22', '2018-04-30 21:58:22', NULL),
(175, 1, 1, 'Commercial test', 'A very talented seller that has professional experience in the music industry.', '23', 'This is a text.', 0, 0, NULL, NULL, 'Deactive', 1, 0, '2018-05-14', 1, 1, '2018-05-14 14:40:38', '2018-05-14 14:40:38', NULL),
(179, 1, 16, 'dfsdf', 'sdf', '645', 'sdf', 0, 2, NULL, NULL, 'Active', 0, 0, '2018-07-09', 1, 1, '2018-07-08 19:04:41', '2018-07-08 19:04:41', NULL),
(180, 1, 1, 'Awesomw writing', 'gdg', '10', 'fcvbb', 0, 16, 'http:/facebook.com', 'http:/facebook.com', 'Active', 0, 0, '2018-08-02', 1, 1, '2018-08-02 00:22:03', '2018-08-02 00:22:03', 'http:/facebook.com'),
(181, 1, 1, 'qqqq updated', 'updated', '100', 'updated', 0, 10, NULL, NULL, 'Active', 0, 0, '2018-08-06', 1, 1, '2018-08-06 03:12:45', '2018-08-06 03:12:45', NULL),
(182, 1, 1, '2 video', NULL, '100', NULL, 0, 3, NULL, NULL, 'Active', 0, 0, '2018-08-06', 1, 1, '2018-08-06 08:08:00', '2018-08-06 08:08:00', NULL),
(183, 1, 14, 'cosmetic VIDEO PREVIEW', 'VIDEO PREVIEW', '99', 'VIDEO PREVIEW', 0, 5, NULL, NULL, 'Active', 0, 0, '2018-08-07', 1, 1, '2018-08-07 08:08:56', '2018-08-07 08:08:56', NULL),
(230, 1, 2, 'Entertainment Night', 'check loader for multiple file', '222', 'check loader for multiple file', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-08-29', 1, 1, '2018-08-29 04:42:12', '2018-08-29 04:42:12', NULL),
(231, 1, 10, 'Science Fiction', 'videoUploader', '1452', 'videoUploader', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-08-29', 1, 1, '2018-08-29 04:45:45', '2018-08-29 04:45:45', NULL),
(232, 1, 1, 'pdf video', 'console.log("loader from appProducts");', '1452', 'hello ', 0, 0, NULL, NULL, 'Deactive', 0, 0, '2018-08-29', 1, 1, '2018-08-29 04:49:07', '2018-08-29 04:49:07', NULL),
(233, 1, 6, 'loader for all', 'bbbbbbbbbb', '1452', 'aaaaaa', 0, 0, NULL, NULL, 'Deactive', 0, 0, '2018-08-29', 1, 1, '2018-08-29 04:53:33', '2018-08-29 04:53:33', NULL),
(234, 1, 1, 'test', NULL, '12', NULL, 0, 78, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 04:55:31', '2018-08-29 04:55:31', NULL),
(235, 1, 4, 'My Music', 'Test data.', '23', '1.Bio Information of Product\n2. Bio Information of Seller', 0, 75, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 05:05:08', '2018-08-29 05:05:08', NULL),
(236, 1, 5, 'wild life photography', NULL, '44', NULL, 5, 25, NULL, NULL, 'Active', 1, 0, '2018-08-29', 1, 1, '2018-08-29 05:07:44', '2018-08-29 05:07:44', NULL),
(238, 1, 13, 'Talent hunt', 'addeddsadd asdsadsad\'assdkasnlkdaskhd $sfgahlk\'laa', '123', 'added asdasdasdsads dssadasd asdsa', 0, 0, NULL, NULL, 'Deactive', 1, 0, '2018-08-29', 1, 1, '2018-08-29 05:42:37', '2018-08-29 05:42:37', NULL),
(244, 1, 1, 'Writer', 'Writer', '120', 'Writer', 0, 11, NULL, NULL, 'Deactive', 1, 0, '2018-09-03', 1, 1, '2018-09-03 00:55:17', '2018-09-03 00:55:17', NULL),
(250, 1, 6, 'I can make you laugh', 'Welcome to FutureStarr', '1000', 'Becoming a professional tattoo artist demands a lot of time, talent, and tenacity. Well-known and sought-after experts in this field go through numerous hurdles before they achieve their goal. The good news is that there are tried and true methods on how you can become a respected and certified body arts specialist. \n\nSurefire Ways to Become a Professional Tattoo Artist\n\nLearning the trade and continuously improving your craft as a tattoo artist is an ongoing process. There are new trends and techniques about body arts that debut in its niche every now and then. \n\nHere are some of the important ways to help you have a solid stronghold for a successful career:\n\nUnderstand your Artwork\n\nTattoos are not just about inking the body with graphics and images. As a certified tattoo artist, you need to know the heart and soul of your artwork. Tattooing is a craft and not just superficial skin arts. The best way to establish your career as a tattoo artist is to imbibe a passionate drive in learning tattoos from their origins to the latest trends. \n\nBe Versatile in your Talent', 0, 24, 'https://www.facebook.com/profile.php?id=100009493880629', NULL, 'Active', 1, 0, '2018-09-09', 1, 1, '2018-09-08 21:35:07', '2018-09-08 21:35:07', NULL),
(254, 1, 8, 'Fitness 2', NULL, '1212', NULL, 0, 5, NULL, NULL, 'Deactive', 1, 0, '2018-09-10', 1, 1, '2018-09-10 00:20:34', '2018-09-10 00:20:34', NULL),
(257, 1, 13, 'Mathematics', NULL, '456', 'Mathematics information', 0, 15, NULL, NULL, 'Active', 1, 0, '2018-09-10', 1, 1, '2018-09-10 00:43:03', '2018-09-10 00:43:03', NULL),
(258, 1, 7, 'Car modal', NULL, '2000', 'A car model (or automobile model or model of car) is the name used by a manufacturer to market a range of similar cars. The way that car manufacturers group their product range into models varies between manufacturers.\n\nA model may also be referred to as a nameplate, specifically when referring to the product from the point of view of the manufacturer, especially a model over time. For example, the Chevrolet Suburban is the oldest automobile nameplate in continuous production, dating to 1934 (1935 model year), while the Chrysler New Yorker was (until its demise in 1996) the oldest North American car nameplate. "Nameplate" is also sometimes used more loosely, however, to refer to a brand or division of larger company (e.g., GMC), rather than a specific model.Given that the interior equipment, upholstery and exterior trim is usually determined by the trim level, the car model often defines the styling theme and platform that is used.[1][2][3][4] The model also defines the body style(s) and engine choice(s).[5]\n\nSome models have only one body style (e.g. the Mazda 2 hatchback),[6] while other models are produced in several body styles (e.g. the Audi A3, which has been produced in hatchback, sedan and convertible body styles).[7] Similarly, some models have a single engine/powertrain specification available (eg the Chevrolet Volt), while other models have multiple powertrains available (eg the Ford Mustang, which has been produced with inline-4, V6 and V8 engines).[8]', 0, 1, NULL, NULL, 'Deactive', 1, 0, '2018-09-10', 1, 1, '2018-09-10 08:07:08', '2018-09-10 08:07:08', NULL),
(267, 1, 2, 'abc', 'aaa', '50', 'aaa', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-10-22', 1, 1, '2018-10-22 07:30:56', '2018-10-22 07:30:56', NULL),
(268, 1, 2, 'div', 'di', '10', 'aaa', 0, 0, NULL, NULL, 'Active', 0, 0, '2019-03-09', 1, 1, '2019-03-09 07:51:31', '2019-03-09 07:51:31', NULL),
(269, 1, 1, 'This is testing for author', 'Testing products with images..', '100', 'Testing products with images..', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-11-11', 1, 1, '2018-11-11 04:34:48', '2018-11-11 04:34:48', NULL),
(270, 1, 2, 'This is a test commercial', 'Diligent, hardworking talent individual with a passion to be successful in all genres of talent.', '20', 'Hello, please disregard this product. DO NOT PURCHASE.', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-11-12', 1, 1, '2018-11-11 18:38:46', '2018-11-11 18:38:46', NULL),
(271, 1, 2, 'Test', 'Test', '0', 'Test', 0, 0, NULL, NULL, 'Active', 0, 0, '2018-11-12', 1, 1, '2018-11-11 18:42:51', '2018-11-11 18:42:51', NULL),
(272, 1, 2, 'music system', 'music', '10', 'music', 0, 58, NULL, NULL, 'Active', 1, 0, '2018-11-14', 1, 1, '2018-11-14 08:08:28', '2018-11-14 08:08:28', NULL),
(273, 1, 1, 'aa', 'aa', '100', 'aa', 0, 13, NULL, NULL, 'Active', 1, 0, '2018-11-14', 1, 1, '2018-11-14 08:55:31', '2018-11-14 08:55:31', NULL),
(274, 1, 4, 'Music is "love" is music', 'Bio informations for product', '250', 'Bio information for product', 0, 4, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 01:09:10', '2019-03-26 01:09:10', 'https://www.instagram.com/'),
(275, 1, 12, 'Food compotistions', 'Nutrition.gov is a USDA-sponsored website that offers credible information to help you make healthful eating choices.', '100', 'Nutrition.gov is a USDA-sponsored website that offers credible information to help you make healthful eating choices.', 0, 0, 'https://twitter.com/', 'https://twitter.com/', 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 01:35:48', '2019-03-26 01:35:48', 'https://twitter.com/'),
(276, 1, 11, 'food is first love', '“All you need is love. But a little chocolate now and then doesn\'t hurt.', '300', '“All you need is love. But a little chocolate now and then doesn\'t hurt.', 0, 3, NULL, NULL, 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 01:59:55', '2019-03-26 01:59:55', NULL),
(277, 1, 11, 'I love you like a fat kid loves cake', 'Ask not what you can do for your country. Ask what’s for lunch', '300', 'One cannot think well, love well, sleep well, if one has not dined well', 0, 1, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 03:15:52', '2019-03-26 03:15:52', 'https://www.instagram.com/'),
(278, 1, 5, 'photography', 'photography love', '400', 'phohtography love', 0, 0, 'https://www.facebook.com/', 'https://twitter.com', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 04:09:13', '2019-03-26 04:09:13', 'https://www.instagram.com/'),
(279, 1, 2, 'Fun is entertainment', 'bio information of seller', '500', 'bio information of product', 0, 0, NULL, NULL, 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 04:11:15', '2019-03-26 04:11:15', NULL),
(280, 1, 10, 'science', 'upload audion or video here', '10', 'upload image or video here', 0, 0, NULL, NULL, 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 04:33:17', '2019-03-26 04:33:17', NULL),
(281, 1, 14, 'cosmetics', 'bio information o fseller', '20', 'bio informations', 0, 2, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 04:55:56', '2019-03-26 04:55:56', 'https://www.instagram.com/'),
(282, 1, 8, 'fitness', 'this is for seller', '30', 'this fields for product', 0, 0, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 0, 0, '2019-03-26', 1, 1, '2019-03-26 05:56:00', '2019-03-26 05:56:00', 'https://www.instagram.com/'),
(283, 1, 6, 'Just for comedy', 'seller', '50', 'product', 0, 6, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 05:58:08', '2019-03-26 05:58:08', 'https://www.instagram.com/'),
(284, 1, 7, 'modeling', 'for seller', '40', 'fo rproduct', 0, 6, 'https://www.facebook.com/', 'https://twitter.com/', 'Active', 1, 0, '2019-03-26', 1, 1, '2019-03-26 06:02:02', '2019-03-26 06:02:02', 'https://www.instagram.com/'),
(285, 1, 4, 'test', NULL, '1', NULL, 0, 4, NULL, NULL, 'Active', 1, 0, '2019-04-09', 1, 1, '2019-04-09 04:30:29', '2019-04-09 04:30:29', NULL),
(286, 1, 5, 'Wild Photographer 2', 'I have 5+ years experience in wild photography about birds update', '1210', 'This is my best photograph shot of product. Here you can see an amazing birtd which is starting fly update', 0, 2, NULL, NULL, 'Active', 1, 0, '2019-04-10', 1, 1, '2019-04-10 12:33:52', '2019-04-10 12:33:52', NULL),
(287, 1, 14, 'Cosmetic HD Bridal Mackup', 'Products Used :\nFace:\nMakeupRevolution Ultra Sculpt & Contour Kit ( Ultra Fair C01)\n\nMusic : Official by YOUTUBE\n♥♥♥ THANK YOU SO MUCH FOR WATCHING ♥♥♥\nthe Balm Balm Voyage Vol. 2 pallete\ntheBalm Mary Lou Manizer Luminizer', '500', 'Products Used :\nFace:\nMakeupRevolution Ultra Sculpt & Contour Kit ( Ultra Fair C01)\n\nMusic : Official by YOUTUBE\n♥♥♥ THANK YOU SO MUCH FOR WATCHING ♥♥♥\nthe Balm Balm Voyage Vol. 2 pallete\ntheBalm Mary Lou Manizer Luminizer', 0, 2, 'facebook.com/test-fb', 'twitter.com/twitter', 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 06:56:00', '2019-04-21 06:56:00', 'instagram.com/test-insta'),
(288, 1, 4, 'Pop Music Band', 'music bio information', '500', 'Popup music details', 0, 1, NULL, NULL, 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 13:35:14', '2019-04-21 13:35:14', NULL),
(289, 1, 13, 'Methamatics teacher', 'personal inforamation of mehamatics', '100', 'Methatmatics teacher', 0, 0, NULL, NULL, 'Active', 0, 0, '2019-04-21', 1, 1, '2019-04-21 14:13:32', '2019-04-21 14:13:32', NULL),
(290, 1, 15, 'Classic feshion design', 'classic fesign design product', '500', 'classic fesign design product', 0, 1, NULL, NULL, 'Active', 1, 0, '2019-04-21', 1, 1, '2019-04-21 14:27:09', '2019-04-21 14:27:09', NULL),
(291, 1, 4, 'This is a new Test', 'I\'m just an entrepreneur from the dirty south baby!', '1', 'Teach online and reach millions of students today. It\'s a great way to earn money, build your online following, and give back.', 0, 2, NULL, NULL, 'Active', 1, 0, '2019-04-23', 1, 1, '2019-04-23 11:47:39', '2019-04-23 11:47:39', 'https://www.instagram.com/p/BwmVGY-DzkC/');

-- --------------------------------------------------------

--
-- Table structure for table `talent_catagories`
--

CREATE TABLE `talent_catagories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_main_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_detailed_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catagory_detailed_icon_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tarending_catagory_sidebar_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `talent_catagories`
--

INSERT INTO `talent_catagories` (`id`, `name`, `catagory_image_path`, `catagory_desc`, `catagory_main_banner`, `catagory_banner`, `catagory_detailed_banner`, `catagory_detailed_icon_img`, `tarending_catagory_sidebar_icon`, `created_at`, `updated_at`) VALUES
(1, 'Author', 'talent-mall-category-images/talentmallpage-categ-Author.jpg', '<p>Whether it comes to being an established author or an aspiring one promoting and selling their works; \r\nwriting has never been easy for anyone. What if you find a platform where you can share your vision as well\r\nas your quality work to the audience around the globe? Future Starr empowers you with one of such high-end and seamless platform.</p> \r\n<p><strong>Promote and Sell Your Work</strong></p>\r\n<p>You can sell any type of books including Fiction, Non-fiction, Cook Books, Travel Books, Children’s Books, \r\nBusiness Books, Comic Books and so on. You just need to upload your work with your complete author profile and \r\nget ready for a fan community you often dreamed of. Just upload your literary work for once and deliver it often. </p>\r\n<p><strong>Approach for Audience</strong></p>\r\n<p>Through Future Starr, reach your global audience as soon as you are finished up with your work. We at, \r\nFuture Starr, know how much time and efforts you have to put in to create your work so we help you to accomplish \r\nreal verified sales for your books, through our massive platform.</p> \r\n<p><strong>Be A Market Pro</strong></p>\r\n<p>So Sign up with future Starr, upload your work, make a great impact and target and reach millions of \r\nreaders/audience worldwide. Our user-focused platform will help raise your sales and popularity.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Author.jpg', 'talent-mall-category-images/author.jpg', 'talent-mall-category-images/detailed/author-banner.jpg', 'talent-mall-category-images/detailed/author-banner3.jpg', 'talent-mall-category-images/icon/blog-author.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(2, 'Entertainment', 'talent-mall-category-images/talentmallpage-categ-Entertainment.jpg', '<p>There is no scarcity of entertainers in the industry, however, the problem arises when they do not get real adequate opportunities to showcase their talent and get united with their audiences and fan community. Future Starr is the one of its kind and unique online marketplace that not only provides the actors with the opportunity to reach out to their audiences but also helps them to build their own online film business.  Just upload your profile and transform your presence.</p>\r\n\r\n<p><strong>Build Up Your Reputation</strong></p>\r\n\r\n<p>Sign up with Future Starr and boost up your reputation among your followers. Our extensive and well-built industry network will also help you in your business venture. Joining hands with Future Starr will authenticate you as a star in the market and as well help you to retain a loyal fan population.</p>\r\n\r\n<p><strong>Be Your Own Boss!</strong></p>\r\n\r\n<p>After you upload your profile on the portal, you can edit and update it from time to time. You can have access to top vendors and get connected with your valuable audiences all over the globe. Showcase your talent and maximize your growth.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Entertainment.jpg', 'talent-mall-category-images/entertenment.jpg', 'talent-mall-category-images/detailed/entertainment-banner.jpg', 'talent-mall-category-images/detailed/entertainment-banner3.jpg', 'talent-mall-category-images/icon/blog-entertainment.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(4, 'Music', 'talent-mall-category-images/talentmallpage-categ-Music.jpg', '<p>Every music composer dreams of getting connected with millions of fans and build up a legacy… Join Future Starr community and let your music speak for you. We let you interact with a worldwide audience and build up a strong fan base just by uploading your music compositions. Future Starr will help you gain new fans and earn huge scales. Sign Up now and get connected with potential fans, like-minded people, and your future customers. With us, you can promote your music everywhere you want. Be a global star with Future Star.</p>\r\n\r\n\r\n<p><strong> Give Yourself A Breakthrough </strong></p>\r\n\r\n<p>We let the emerging musicians thrive by boosting up their creativity and build up a strong fan base. Whatever your genre may be; Punk, Rap, Hip Hop, EDM, Pop, Metal, Rock; just upload your music and promote yourself! </p>\r\n\r\n<p><strong>Boost Up Your Career</strong> </p>\r\n\r\n<p>Uploading your music on Future Starr will help you get connected to various labels, brands, publishers and to a wider fan community. Our industry tie-ups’ will help you access the global music industry. So Sign Up today to create a buzz for your music.</p>\r\n', 'talent-mall-category-images/main-banners/Talent-mall-banner-Music.jpg', 'talent-mall-category-images/music.jpg', 'talent-mall-category-images/detailed/music-banner.jpg', 'talent-mall-category-images/detailed/music-banner3.jpg', 'talent-mall-category-images/icon/blog-music.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(5, 'Photography', 'talent-mall-category-images/talentmallpage-categ-Photography.jpg', '<p>Beauty lies in the eyes of the beholder. This saying can aptly define the fearless job of a photographer. Through his creativity, he can compel the world to see and perceive through his eyes. If you are such an arduous and zealous photographer with a unique collection of your own photos and you want to showcase, promote and sell those quality shots then Future Starr is a platform for you. Future Starr is a global marketplace for the photographers and artists to display, share and sell their images. Whatever your niche may be including travel, fashion, cityscapes landmarks, weddings, portraitures, wildlife or more; you just need to upload your passionate projects and turn it into hard cash.</p>\r\n\r\n<p><strong>Simplify Your Business </strong></p>\r\n\r\n<p>Our state-of-art and easy to use platform will help you to:\r\n<p>* Delight your customers </p>\r\n<p>* Help in growing your business</p>\r\n<p>* Get rewarded for your photography efforts</p>\r\n\r\n<p>Future Starr helps you to meet your clients’ expectations. </p>\r\n\r\n<p><strong> Make Yourself Visible </strong> </p>\r\n\r\n<p>Future Starr will help you to expand your audience and credibility through our photo sharing platform. You can also get connected to other creative professionals like designers, publishers, and marketing departments. </p>\r\n\r\n<p>So whether you are a professional photographer or an amateur, getting linked with Future Starr will ultimately turn your creativity into cash and earn you worldwide clients. </p>', 'talent-mall-category-images/main-banners/Talent-mall-bannePhotography.jpg', 'talent-mall-category-images/photography.jpg', 'talent-mall-category-images/detailed/phography-banner.jpg', 'talent-mall-category-images/detailed/phography-banner3.jpg', 'talent-mall-category-images/icon/blog-photography.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(6, 'Comedy', 'talent-mall-category-images/talentmallpage-categ-Comedy.jpg', '\r\n<p>How to build up a strong fanbase is the biggest fear and challenge that every comedian must face. Various aspiring and promising comedians that were on the rise disappear every year. Organizing a stellar comedy event and having a great comedy timing is not enough to reach the audience. What you need is a platform that not only helps you to reach your target audience but also helps to expand your fanbase. Sign Up with Future Starr and get connected to the global audiences. </p>\r\n\r\n<p><strong>Market the Entertainer in You! </strong></p>\r\n\r\n<p>Upload your best works, events, video clips from your T.V. shows, podcasts and expand your fan community way beyond and be a global star. </p>\r\n\r\n<p><strong>Target Your Fans</strong></p>\r\n<p>Future Starr will give you the opportunity to create a fanbase who comes again and again to you. With us, convert your online audience into download purchases. </p>\r\n\r\n<p><strong>Advertise Yourself</strong></p>\r\n\r\n<p>You can also advertise your approaching events through Future Starr promote your comedy event and get the word out to the right people and make yourself successful. </p>', 'talent-mall-category-images/main-banners/Talent-mall-banne-Comedy.jpg', 'talent-mall-category-images/comedy.jpg', 'talent-mall-category-images/detailed/commedy-banner.jpg', 'talent-mall-category-images/detailed/commedy-banner3.jpg', 'talent-mall-category-images/icon/blog-comedy.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(7, 'Model', 'talent-mall-category-images/talentmallpage-categ-Model.jpg', '<p>Striking visual presentations and charismatic online portfolios are absolute musts for models if they want themselves to get noticed. To create a high-quality online modeling portfolio and promote yourself with Future Starr, you want to make the difference and outshine in the modeling industry. And for this promotion, you need not spend big a fortune; just upload your best modeling videos and pictures and raise your audiences and visibility. Upload your photos depicting your different moods with diverse looks, backgrounds, and poses and get yourself noticed by a worldwide audience including; various editors, modeling agencies, bloggers, and fan community while earning extra income.</p>\r\n\r\n<p><strong>Flaunt Your Talent</strong></p>\r\n\r\n<p>With Future Starr, show your competency to your potential clients, scouts, fans and employers. Upload your various projects and photoshoots showcasing your lovely features and artistic vision and make your presence impressive.\r\n</p>\r\n<p><strong>Share Your Uniqueness</strong></p>\r\n\r\n<p>Share the details that are unique to you; like your eye color, skin tone or tattoos, height etc. and spice up your online modelling portfolio.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Model.jpg', 'talent-mall-category-images/modeling.jpg', 'talent-mall-category-images/detailed/model-banner.jpg', 'talent-mall-category-images/detailed/model-banner3.jpg', 'talent-mall-category-images/icon/blog-model.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(8, 'Fitness', 'talent-mall-category-images/talentmallpage-categ-Fitness.jpg', '\r\n<p>Either you are a personal trainer or fitness guru and you are zealous to share your expertise by promoting yourself globally then you need a platform that can get you connected with the global audience and help you attract clients worldwide. Join hands with Future Starr! Get an edge over the competition and give a much-needed boost to your fitness training career. Just upload your instructional videos, or video tutorials showcasing your training sessions, nutrition or diet plans, group or customize programs offered and track your career progress in real-time. With Future Starr, you can market your services or fitness plans effortlessly. Also, you can earn huge fan community or clients and revolutionize your business.</p>\r\n\r\n<p><strong>Grow your Business</strong></p>\r\n\r\n<p>Future Starr will help you promote and sell your services of fitness products instantly. We help you get connected with a huge digital fitness community including other fitness trainers as well.</p>\r\n\r\n<p><strong>Stand Out from the Rest</strong></p>\r\n<p>Upload your video presentations, clients’ testimonials, your products’ lists, service packages, initial guidance videos or one-on-one training videos and earn immense profits along with a loyal fanbase.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Fitness.jpg', 'talent-mall-category-images/health.jpg', 'talent-mall-category-images/detailed/health-banner.jpg', 'talent-mall-category-images/detailed/health-banner3.jpg', 'talent-mall-category-images/icon/blog-fitness.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(9, 'National Geographic', 'talent-mall-category-images/talentmallpage-categ-National-Geographic.jpg', '	<p>Whether you are a dedicated environmental activist, start-up, or an international organization that wants to create a buzz in the society about environmental issues like global warming, saving oceans, nuclear testing or many other threats, Join hand with Future Starr to make mother earth a greener, fairer, and environmentally sustainable place to live.  To promote your ideas you just need to upload videos of your campaigns, your investigation, and the solutions you want to get executed. Empower yourself or your organization with the collective power audience or people you reach through Future Starr.</p>\r\n\r\n<p><strong>Get Connected </strong></p>\r\n\r\n<p>Future Starr enables you to get connected with the global audience. You can create public awareness and can encourage more and more people to donate to noble causes. You can promote the research you have made and your educational programs and get linked up with a like-minded community of experts, scientists and researchers. </p>\r\n\r\n<p><strong>Create a Global Platform</strong></p>\r\n\r\n<p>Upload videos of your training sessions, meetings, rallies or marches and engage the global audience to create awareness about various environmental issues and attract non-profit organizations, or other activists who can support you. This can also help you raise donations for your campaign. Stop the environmental destruction through direct communication with a world audience.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-National-Geographic.jpg', 'talent-mall-category-images/nationalgeography.jpg', 'talent-mall-category-images/detailed/national-geography-banner.jpg', 'talent-mall-category-images/detailed/national-geography-banner3.jpg', 'talent-mall-category-images/icon/blog-ng.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(10, 'Science', 'talent-mall-category-images/talentmallpage-categ-Science.jpg', '<p>Science brings into minds the wacky physics equations, bubbling analysis devices, telescope heredity, ecology, and many more puzzling ideas. If you are a geek at all these and want to provide virtual teachers aid to parents or students; Future Starr is the platform. Through us, we offer design-board learning strategies, lesson plans, illustrations, to make the student understand various scientific concepts. Upload your lesson plans related to biology, philosophy, earth science, astronomy or anthropology.\r\n</p>\r\n<p><strong>Provide Hands-on Experience</strong></p>\r\n\r\n<p>Future Starr let you provide online hand on experience to the students. You can upload animations or quizzes that you have developed related to different topics of science and keep your clients or students entertained and engaged as well.</p>\r\n\r\n<p><strong>Upload Your Classroom Teaching</strong></p>\r\n\r\n<p>Upload your actual classroom teaching visual clips or experiments in biology, chemistry, engineering or you can also upload charts graphs, or tables to provide detailed knowledge about various scientific concepts.  Future Starr enables you to provide a virtual lab to your students. Be accessible and searchable with Future Starr.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Science.jpg', 'talent-mall-category-images/scince.jpg', 'talent-mall-category-images/detailed/science-banner.jpg', 'talent-mall-category-images/detailed/science-banner3.jpg', 'talent-mall-category-images/icon/blog-scince.png', '2017-11-06 16:05:01', '2017-11-06 16:05:01'),
(11, 'Food', 'talent-mall-category-images/talentmallpage-categ-Food.jpg', '<p>If you are food enthusiasts and want to be a tomorrow’s trendsetter in the culinary industry through your exquisite tempting dishes, get connected with Future Starr. Promote and share your culinary expertise with a like-minded community of cooks, culinary fans, and food lovers. Just upload your video clips, cooking tutorials, share the pictures of your recipes, food items or other food-related activities, and Future Starr will provide you a platform where you can share, promote and interact with your fan community. Not only chefs even the restaurants can market themselves through our diversified platforms. </p>\r\n\r\n<p><strong>Advertise Your Skills</strong></p>\r\n<p>Through Future Starr, you can exhibit your culinary skills to not only to your fan community but also to culinary professionals including restaurant owners, hoteliers, executive chefs or sommeliers.</p>\r\n\r\n<p><strong>Grow with Future Starr</strong></p>\r\n\r\n<p>Create galleries through pictures of your recipes  video tutorials and reach worldwide food enthusiasts. Raise your professional culinary standards with Future Starr’s digital credential and realize your full potential.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Food.jpg', 'talent-mall-category-images/food.jpg', 'talent-mall-category-images/detailed/food-banner.jpg', 'talent-mall-category-images/detailed/food-banner3.jpg', 'talent-mall-category-images/icon/blog-food.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00'),
(12, 'Nutrition', 'talent-mall-category-images/talentmallpage-categ-Nutrition.jpg', '<p>Whether you are a registered dietitian, nutritionist, registered dietetic technician, or any other registered dietetic professional and you are committed to shaping the very core of your clients while positively influencing their health then Future Starr is the platform that can help you to accomplish your goal. With Future Starr, you can target your specialized audience by uploading your blogs, or videos to share your healthy cooking recipes or your opinions about some important nutrition topics. Share your visually creative outlet to promote yourself. Keep your clients engaged with your beautifully displayed healthy recipes.</p>\r\n\r\n<p><strong>Forward Your Expert Opinion</strong></p> \r\n\r\n<p>Future Starr enables the nutritionists and dietitians to forward their views on foods, environmental issues, organic foods, science-based nutrition information or healthy living topics. </p>\r\n\r\n <p><strong>Guide Clients to Better Nutrition </strong></p>\r\n<p>With Future Starr advertising your nutritional plans, your health packed recipes can promote your views regarding various issues including weight control, heart health diabetes or reducing stress. We also help you to get connected with other nutrition experts and health professionals.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Nutrition.jpg', 'talent-mall-category-images/nutrition.jpg', 'talent-mall-category-images/detailed/Nutrition-banner.jpg', 'talent-mall-category-images/detailed/Nutrition-banner3.jpg', 'talent-mall-category-images/icon/blog-nutrition.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00'),
(13, 'Mathematics', 'talent-mall-category-images/talentmallpage-categ-Mathematics.jpg', '<p>If you are a calculus or trigonometry Wizz or an expert in writing tricky math problems and want to be more accessible to your students, get connected with Future Starr. We will get you connected with the people who need math experts to teach them. Just upload your tutoring videos showcasing your expertise and be a part of the best online marketplace. Express your math knowledge and skills with Future Starr and get rid of costly advertisements.</p>\r\n\r\n\r\n<p><strong>Direct Communication</strong></p>\r\n<p>By uploading your videos on Future Star, you can establish an easy and direct communication with your students worldwide. Just upload your availability along with your tutoring profile and experience new ways of online tutoring.</p>\r\n\r\n\r\n<p><strong>Earn Real Results</strong></p>\r\n\r\n<p>Future Starr enables you to advertise your skills anywhere in the world or in your country while never leaving your place or home and be a member of a vetted experts’ community. Just upload your work and help the students to understand the importance of math in a better and more intellectual way.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Mathematics.jpg', 'talent-mall-category-images/methamatics.jpg', 'talent-mall-category-images/detailed/mathematics-banner.jpg', 'talent-mall-category-images/detailed/mathematics-banner3.jpg', 'talent-mall-category-images/icon/blog-mathematics.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00'),
(14, 'Cosmetics', 'talent-mall-category-images/talentmallpage-categ-Cosmetics.jpg', '<P>If you dream of beauty and want to establish yourself as a beauty trailblazer in the market through your innovative and revolutionary beauty retail products, sign up with Future Starr. Future Starr will help you make a powerful presence around the world. Upload your range of beauty products and expose them to global clients or merchandising teams as well. You can directly interact with them through our platform. </p>\r\n\r\n<p><strong>Grow With US</strong></p>\r\n\r\n<p>Whether you deal in cosmetics, fragrance, skin care products, hair care products or salon services; linking up with Future Starr will earn your ultimate loyal fanbase and clientele. Showcase the world the beauty service you provide or exclusive offers they can get or engross them in your products that they will love and enjoy using.</p>\r\n\r\n<p><strong>Globalize your Business</strong></p>\r\n\r\n<p>With Future Starr creates a worldwide network of beauty through your unique and exotic beauty products. We give you the opportunity to develop a worldwide market for your new beauty products which in return will also grow your business at the global level. Future Starr fulfills your dreams and also make the dreams of your clients come true.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Cosmetics.jpg', 'talent-mall-category-images/cosmetics.jpg', 'talent-mall-category-images/detailed/cosmetics-banner.jpg', 'talent-mall-category-images/detailed/cosmetics-banner3.jpg', 'talent-mall-category-images/icon/blog-cosmetics.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00'),
(15, 'Fashion designer', 'talent-mall-category-images/talentmallpage-categ-Fashion-Designer.jpg', '<p>If you have a fashion brand with original or unique concept designs while focusing on detailed refinements to satisfy your customers; Join hands with Future Starr. We at Future Starr provide you with bold solutions that not only allow you to stand apart from the crowd but also helps you to reach a much wider and global clientele. We offer you the opportunity to provide your customers with the same care and one-on-one service online as they get on the stores and began their shopping experience. Upload your products related videos, infographics, photos and improve your customers’ experience.\n</p>\n\n<p><b>Go An Extra Mile</b></p>\n\n<p>Through Future Starr, you can bring your calculations to the customers all over the world while providing them personalized service. Upload your clothing’ or your catalogs, displaying size and colors available and offer bespoke service to your customers. </p>\n\n<p><b>Launch your Brand with Future Starr</b></p> \n\n<p>Upload your business campaigns, share blog posts or user guides to update your customers about new dressing trends or lifestyles. Share the competitions you organized or celebrations you made and make your customers your partners and earn a loyal fanbase.</p>', 'talent-mall-category-images/main-banners/Talent-mall-banner-Fashion-designer.jpg', 'talent-mall-category-images/fashion.jpg', 'talent-mall-category-images/detailed/fashion-banner.jpg', 'talent-mall-category-images/detailed/fashion-banner3.jpg', 'talent-mall-category-images/icon/blog-fashion.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00'),
(16, 'Tattoo Artist', 'talent-mall-category-images/talentmallpage-categ-tattoo.jpg', '<p>If you are a certified, professionally trained and licensed tattoo artist, who are determined to build and improve their tattoo art along with wanting to raise a reputable platform for their art, then Future Starr is your ultimate destination. Here we would facilitate your first steps in your journey towards success. Future Starr helps the tattoo artists, studios, and conventions as well to work and operate more efficiently. Future Starr is a one-stop destination for tattoo art and lifestyle. We provide artists with the opportunity to get connected with new clients as well as help them to find new tattoo ideas by linking them up with another artist for their business growth.\r\n</p>\r\n\r\n<p><strong>Revolutionize Your Tattoo Space</strong></p>\r\n\r\n<p>Through Future Starr, you can get connected with reputable tattoo studios, can book your consultations easily and that too around the world. Our global platform will bring you closer and as well make you accessible to the global audience.</p>\r\n\r\n<p><strong>Let Your Talent All Hang Out</strong></p>\r\n\r\n<p>Upload your curated art galleries, articles, blogs, client’s reviews, artists’ recommendations, tattoo making videos and let the world know that tattoo art is much higher than ink on skin and let them appreciate and share your tattoo art.</p>', 'talent-mall-category-images/main-banners/talentmallpage-tattos.jpg', 'talent-mall-category-images/tattoo.jpg', 'talent-mall-category-images/detailed/Tattoo-Artist-banner.jpg', 'talent-mall-category-images/detailed/tatoo_image.jpg', 'talent-mall-category-images/icon/blog-tattoo.png', '2017-11-29 07:30:00', '2017-11-29 07:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visibilty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `automatic_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addreauto_replyss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verified` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `vacation_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'off',
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `username`, `display_name`, `experience_level`, `visibilty`, `phone`, `city`, `state`, `zip_code`, `automatic_message`, `addreauto_replyss`, `email`, `profile_pic`, `password`, `remember_token`, `address`, `email_verified_at`, `email_verified`, `vacation_mode`, `description`, `created_at`, `updated_at`) VALUES
(1, 3, 'Ashok', 'Kumar', 'ashok', 'Ashok', NULL, NULL, '1234667899', 'mohali', 'punjab', '123456', NULL, NULL, 'ashoka326@gmail.com', NULL, '$2y$10$xraIoG0AvjURJZZL.l11v.lMsRz/B7bxbtaA/NcYCZy9aytRrgyqu', 'htTQqndJtEVBm85i4VStWfzjYCaJolHGoKf46dTbMMnRF1uO8L78DoCNkSaa', 'test', NULL, 'no', 'off', 'test', '2019-07-14 08:53:37', '2019-07-14 08:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `name`, `permission`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'all', '2019-07-14 08:52:16', NULL),
(2, 'manager', 'read,write', '2019-07-14 08:52:16', NULL),
(3, 'buyer', 'read,write', '2019-07-14 08:52:16', NULL),
(4, 'seller', 'read,write', '2019-07-14 08:52:16', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_contents`
--
ALTER TABLE `blog_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_contents_cat_id_foreign` (`cat_id`),
  ADD KEY `blog_contents_created_by_foreign` (`created_by`),
  ADD KEY `blog_contents_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `commercial_media`
--
ALTER TABLE `commercial_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commercial_media_talent_id_foreign` (`talent_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample_media`
--
ALTER TABLE `sample_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sample_media_talent_id_foreign` (`talent_id`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talents_user_id_foreign` (`user_id`),
  ADD KEY `talents_category_id_foreign` (`catagory_id`),
  ADD KEY `talents_created_by_foreign` (`created_by`),
  ADD KEY `talents_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `talent_catagories`
--
ALTER TABLE `talent_catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_contents`
--
ALTER TABLE `blog_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `commercial_media`
--
ALTER TABLE `commercial_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sample_media`
--
ALTER TABLE `sample_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=292;
--
-- AUTO_INCREMENT for table `talent_catagories`
--
ALTER TABLE `talent_catagories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_contents`
--
ALTER TABLE `blog_contents`
  ADD CONSTRAINT `blog_contents_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `talent_catagories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_contents_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_contents_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `talents`
--
ALTER TABLE `talents`
  ADD CONSTRAINT `talents_category_id_foreign` FOREIGN KEY (`catagory_id`) REFERENCES `talent_catagories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talents_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talents_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `talents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
