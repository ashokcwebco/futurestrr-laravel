<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TalentCatagory;
use App\Models\Talents;

class TalentMallContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catagories = TalentCatagory::all();
        return view('frontend.talentmall.index',compact('catagories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catagories = TalentCatagory::all();
        $talents = Talents::with('user','commercialMedia','sampleMedia')->whereCatagoryId($id)->paginate(10);
        return view('frontend.talentmall.info',compact('catagories','id','talents'));
    }


    /**
     * Display the specified resource with ajax. 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTalentByCat(Request $request)
    {
        if($request->ajax()){
            $talents = Talents::with('user','commercialMedia')->whereIn('catagory_id',$request->selectedCat)->paginate(10);
            return view('frontend.talentmall.ajax-list',compact('talents'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
