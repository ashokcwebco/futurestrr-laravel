<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Talents extends Model
{
    //

    public function user() {
		return $this->belongsTo('App\User','user_id');
  }
  
  public function commercialMedia() {
		return $this->hasMany(CommercialMedia::Class,'talent_id');
  }

  public function sampleMedia() {
		return $this->hasMany(SampleMedia::Class,'talent_id');
  }
  
}
